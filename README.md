# Project Setup and Execution Guide

## BwUniCluster2.0

### Setup

1. Connect to the cluster via SSH

    ```bash
    ssh <username>@uc2.scc.kit.edu
    ```

2. Download the project

    ```bash
    git clone https://gitlab.com/ArvandKaveh/pnn.git
    ```

3. Create a virtual environment

   First, install miniconda by following the
   instructions [here](https://docs.conda.io/projects/miniconda/en/latest/index.html#quick-command-line-install).

    ```bash
    mkdir -p ~/miniconda3
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
    bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
    rm -rf ~/miniconda3/miniconda.sh

    # Initialize conda in your bash shell
    ~/miniconda3/bin/conda init bash
    source ~/.bashrc
    ```

   Then, create a virtual environment and install the required packages:

    ```bash
    cd ~/pnn
    conda create --name cuda python=3.11
    conda activate cuda
    # Install required packages from environment.yml
    conda env update -f environment-bw-uni-cluster-cuda.yml
    # Ensure setup completed and install additional packages
    # Also ensure that the given path to miniconda in the following script is yours.
    ./script/setup_bw_uni_cluster.sh
    ```

### Usage

1. Running

   Ensure that the scripts start executing on the login node to avoid errors after the job has been submitted to the
2. cluster.

    ```bash
    ./run_YOUR_SCRIPT.sh
    ```

   Ensure that the script is executable:

    ```bash
    chmod +x run_YOUR_SCRIPT.sh
    ```

2. Submitting to the cluster

   Once you are sure that the script is executable and runs without errors, you can submit it to the cluster.

   Make sure, to have set the correct `SBATCH` parameters in the script, such as `timeouts`, required cluster `cores`
   and `GPUs` and the `job-name`.

    ```bash
    #SBATCH --job-name=process_audio                # job name
    #SBATCH --partition=gpu_4                       # single, gpu_4
    #SBATCH --time=02:00:00                         # wall-clock time limit  
    #SBATCH --mem=200000                            # in MB check limits per node
    #SBATCH --nodes=1                               # number of nodes to be used
    #SBATCH --cpus-per-task=1                       # number of CPUs required per MPI task
    #SBATCH --ntasks-per-node=1                     # maximum count of tasks per node
    #SBATCH --mail-type=ALL                         # Notify user by email when certain event types occur.
    #SBATCH --gres=gpu:4                            # number of GPUs required per node 
    #SBATCH --output=../../target_location/logs/output_%j.txt   # standard output and error log
    #SBATCH --error=../../target_location/logs/error_%j.txt     # %j is the job id, making each log file unique, therefore not overwriting each other
    ```

   To then submit the script to the cluster, run:

    ```bash
    sbatch run_YOUR_SCRIPT.sh
    ```
   You can also take a look at `/script` folder for some ready scripts.

4. Monitoring

   To monitor the status of your job, run:

    ```bash
    squeue -u <username>
    ```

   To cancel a job, run:

    ```bash
    scancel <job-id>
    ```

7. Downloading the results

   To download the results from the cluster, run:

    ```bash
    scp [-r] <username>@uc2.scc.kit.edu:~/<PATH-ON-REMOTE> <LOCAL-PATH>
    ```

   The `-r` flag is only required if you want to download a directory.

## Other Platforms

### Setup

1. Clone the repository

```bash
git clone https://gitlab.com/ArvandKaveh/pnn.git
```

2. Install miniconda3. Refer to https://docs.anaconda.com/free/miniconda/miniconda-install/ (alternatively you can use
   the pythons virtual environment. We provide documentation for conda environment.)
3. Create a conda environment

    ```bash
   # Change directory to where the repository is located. Then run the following.
    conda create --name pnn python=3.11
    conda activate pnn
    ```

4. Install the pytorch dependent on your platform and hardware. Please refer to: https://pytorch.org/
5. Install the following dependencies:

```bash
# Make sure your conda environment is activated: 
conda activate pnn

conda install -c conda-forge matplotlib
conda install scipy
conda install tensorboard
conda install tqdm
conda install pandas
```

6. (Alternative): If any of the installations with conda did not work. You can install the dependencies also using pip:

```bash
# Make sure your conda environment is activated: 
conda activate pnn

pip install matplotlib
pip install scipy
pip install tensorboard
pip install tqdm
pip install pandas
```

### Usage

This project includes a command-line interface for training models with customizable parameters. To use it, follow the
instructions below.

## Basic Command

Call the command form the base directory, where the repository is located.
If you have problems with the imports, make sure that the PYTHONPATH includes the location of this project:

 ```bash
# First Change directory the repository's location. Then run the following command:
export PYTHONPATH="$PYTHONPATH:$PWD"
```

### Help

Run the help command to see the help statements regarding all the available arguments.

```bash
python -m src.run_training_base -h
```

### Basic Training Command

```bash
python -m src.run_training_base  \
  --home_dir <home_directory>  \
  --generators <generator_names>  \
  --discriminator <discriminator_name> [options]
```

## Arguments

### Mandatory

    --home_dir, -hd: The home directory of the project. (Required)
    --generators, -gens: Space-separated short names of the generators. Available generators are: gen128,
                        gen128t, gen512, gen512t (Required)
    --discriminator, -disc: The short name of the discriminator. Available
                        discriminators are: disc-basic(Required)

## Optional

    --learning_rate, -lr: The learning rate. Default is 0.00003.
    --batch_size, -bs: The batch size. Default is 32.
    --num_epochs, -ne: The number of epochs. Default is 50.
    --noise_dim, -nd: The noise dimension. Default is 100.
    --window_size, -ws: The window size for averaging. Default is 5.
    --is_running_eval, -ire: Whether to run evaluation while training. Default is True.
    --use_spectral_norm, -sn: Enable spectral normalization. Default is False.
    --use_batch_normalisation_generator, -bng: Enable batch normalization in the generator. Default is False.
    --use_leaky_relu_discriminator, -lrd: Use leaky ReLU in the discriminator. Default is False.
    --use_leaky_relu_generator, -lrg: Use leaky ReLU in the generator. Default is False.
    --use_augmentation, -ua: Apply data augmentation. Default is False.
    --log_tensorboard, -tb: Log training progress to TensorBoard. Default is False.
    --beta1, -b1: The exponential decay rate for the first moment estimates in Adam optimizer. Default is 0.9.
    --beta2, -b2: The exponential decay rate for the second moment estimates in Adam optimizer. Default is 0.999.
    --criterion, -ce: The criterion to use for loss calculation. Default is bce_logits. Available criteria are:{'short name':
                        'bce_logits', 'description': 'Binary cross
                        entropy loss with logits'}, {'short name':
                        'mse', 'description': 'Mean squared error'}
    --label_smoothing_factor, -lsf: The label smoothing factor. Default is 1.0.
    --use_minibatch_discriminator, -mbd: Use minibatch discriminator. Default is False.

## Example Command

```bash
python -m src.run_training_base \
  --home_dir ./res/test_run \
  --num_epochs 20 \
  --is_running_eval true \
  --generators "gen128" "gen512" \
  --discriminator "disc-basic" \
  -mbd true \
  -b1 0.5 \
  -lrd true \
  -lsf 0.9 \
  --use_spectral_norm true
```

This command configures the training process with specific parameters, including the use of minibatch discrimination,
modified optimizer rates.

Please note that the `results_utils.py` need to be updated if you add new results (by training new models).

Use, the functions available in the `image_generation.py` and `plotter.py` to generate images and/or plot metrics of the
results. 
