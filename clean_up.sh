#!/bin/bash

# Define the root directory
ROOT_DIR="/Users/arvandkaveh/Projects/kit/pnn/res"

# Define the directories to delete
#DIRS_TO_DELETE=("evals" "img" "models" "best_imgs_grid")
DIRS_TO_DELETE=("best_imgs_grid")

# Loop over each directory to delete
for dir in "${DIRS_TO_DELETE[@]}"; do
    # Find and delete directories
    find "$ROOT_DIR" -name "$dir" -type d -exec rm -rf {} +
    echo "Deleted: $dir"
done

echo "Specified directories have been deleted."
