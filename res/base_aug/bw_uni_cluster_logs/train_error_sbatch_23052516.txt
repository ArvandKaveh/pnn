No modules loaded
[2024-01-15 22:46:49,110] [INFO]: Starting the training process
[2024-01-15 22:46:49,111] [INFO]: With the following arguments: CommandLineArgs(home_dir='/home/kit/stud/ubppd/pnn_results_base_aug', learning_rate=3e-05, batch_size=32, num_epochs=200, noise_dim=100, window_size=5, is_running_eval=True, use_leaky_relu=False, use_spectral_norm=False, use_augmentation=True)
[2024-01-15 22:46:49,111] [INFO]: Loading 100 real images per class from CIFAR-10 dataset
[2024-01-15 22:47:10,064] [INFO]: Shape of a randomly selected real image: torch.Size([3, 299, 299])
[2024-01-15 22:47:10,714] [INFO]: Extracting features
[2024-01-15 22:47:10,714] [INFO]: using device: cuda
[2024-01-15 22:47:12,452] [INFO]: Initializing training for generator: Generator512D02L5
[2024-01-15 22:47:12,455] [INFO]: Trainer initialized with the following parameters:
--------Generator--------
Sequential(
  (0): Linear(in_features=110, out_features=8192, bias=True)
  (1): ReLU()
  (2): Dropout(p=0.2, inplace=False)
  (3): Unflatten(dim=1, unflattened_size=(512, 4, 4))
  (4): Conv2d(512, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (5): ReLU()
  (6): Dropout(p=0.2, inplace=False)
  (7): Upsample(scale_factor=2.0, mode='nearest')
  (8): Conv2d(256, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (9): ReLU()
  (10): Dropout(p=0.2, inplace=False)
  (11): Upsample(scale_factor=2.0, mode='nearest')
  (12): Conv2d(128, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (13): ReLU()
  (14): Dropout(p=0.2, inplace=False)
  (15): Upsample(scale_factor=2.0, mode='nearest')
  (16): Conv2d(64, 3, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (17): Tanh()
)
--------Discriminator--------
Sequential(
  (0): Conv2d(13, 64, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False)
  (1): ReLU()
  (2): Dropout(p=0.2, inplace=False)
  (3): Conv2d(64, 128, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False)
  (4): BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (5): ReLU()
  (6): Dropout(p=0.2, inplace=False)
  (7): Conv2d(128, 256, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), bias=False)
  (8): BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (9): ReLU()
  (10): Dropout(p=0.2, inplace=False)
  (11): Conv2d(256, 1, kernel_size=(4, 4), stride=(1, 1), bias=False)
  (12): Flatten(start_dim=1, end_dim=-1)
)
--------Configurations--------
Config(name='Generator512D02L5', base_path=PosixPath('/home/kit/stud/ubppd/pnn_results_base_aug'), batch_size=32, noise_dim=100, data_path=PosixPath('/home/kit/stud/ubppd/pnn_results_base_aug/Generator512D02L5'), log_path=PosixPath('/home/kit/stud/ubppd/pnn_results_base_aug/Generator512D02L5/evals'), num_classes=10, num_epochs=200, evaluation_num_images_per_class=100, keep_best_models=5, start_evaluating_from_epoch=0, log_tensorboard=False, use_augmentation=True, epsilon=0.1, avg_window_size=5)
criterion: BCEWithLogitsLoss()
Generator optimizer: Adam (
Parameter Group 0
    amsgrad: False
    betas: (0.9, 0.999)
    capturable: False
    differentiable: False
    eps: 1e-08
    foreach: None
    fused: None
    lr: 3e-05
    maximize: False
    weight_decay: 0
)
Discriminator optimizer: Adam (
Parameter Group 0
    amsgrad: False
    betas: (0.9, 0.999)
    capturable: False
    differentiable: False
    eps: 1e-08
    foreach: None
    fused: None
    lr: 3e-05
    maximize: False
    weight_decay: 0
)
torch.cuda.is_available(): True
device: cuda
keep_best_models: 5

[2024-01-15 22:47:12,455] [INFO]: Starting the training process for generator: Generator512D02L5
[2024-01-15 22:47:12,455] [INFO]: Starting epoch 1
[2024-01-15 22:47:12,455] [INFO]: Loading CIFAR10 dataset with batch size 32 and train split
[2024-01-15 22:47:12,455] [INFO]: Using data augmentation
[2024-01-15 22:47:12,455] [INFO]: Normalizing images
[2024-01-15 22:47:51,439] [INFO]: Epoch [1/200]. Time taken for epoch 38 seconds
[2024-01-15 22:47:51,440] [INFO]: Evaluating epoch 1
[2024-01-15 22:47:51,441] [INFO]: Generating 100 images per class
[2024-01-15 22:47:51,465] [INFO]: Finished generating 100 images per class
/home/kit/stud/ubppd/miniconda3/envs/cuda/lib/python3.11/site-packages/torchvision/transforms/functional.py:1603: UserWarning: The default value of the antialias parameter of all the resizing transforms (Resize(), RandomResizedCrop(), etc.) will change from None to True in v0.17, in order to be consistent across the PIL and Tensor backends. To suppress this warning, directly pass antialias=True (recommended, future default), antialias=None (current default, which means False for Tensors and True for PIL), or antialias=False (only works on Tensors - PIL will still use antialiasing). This also applies if you are using the inference transforms from the models weights: update the call to weights.transforms(antialias=True).
  warnings.warn(
[2024-01-15 22:47:51,843] [INFO]: Calculating Inception Score
[2024-01-15 22:47:53,636] [INFO]: Extracting features
[2024-01-15 22:47:53,636] [INFO]: using device: cuda
[2024-01-15 22:47:54,938] [INFO]: Calculating FID
[2024-01-15 22:47:55,963] [INFO]: fid calculated: 2382.538447479722
[2024-01-15 22:47:55,963] [INFO]: Calculating FID
[2024-01-15 22:47:56,494] [INFO]: fid calculated: 2662.920649890014
[2024-01-15 22:47:56,494] [INFO]: Calculating FID
[2024-01-15 22:47:57,016] [INFO]: fid calculated: 2665.4007740944307
[2024-01-15 22:47:57,016] [INFO]: Calculating FID
[2024-01-15 22:47:57,532] [INFO]: fid calculated: 2817.5145248147855
[2024-01-15 22:47:57,532] [INFO]: Calculating FID
[2024-01-15 22:47:58,057] [INFO]: fid calculated: 3155.4593053215467
[2024-01-15 22:47:58,057] [INFO]: Calculating FID
[2024-01-15 22:47:58,582] [INFO]: fid calculated: 2656.931663999826
[2024-01-15 22:47:58,583] [INFO]: Calculating FID
[2024-01-15 22:47:59,104] [INFO]: fid calculated: 3002.942531544444
[2024-01-15 22:47:59,104] [INFO]: Calculating FID
[2024-01-15 22:47:59,641] [INFO]: fid calculated: 3009.109202177886
[2024-01-15 22:47:59,641] [INFO]: Calculating FID
[2024-01-15 22:48:00,178] [INFO]: fid calculated: 2670.7179591839376
[2024-01-15 22:48:00,178] [INFO]: Calculating FID
[2024-01-15 22:48:00,714] [INFO]: fid calculated: 2598.0070181402057
[2024-01-15 22:48:00,714] [INFO]: Calculating FID
[2024-01-15 22:48:01,221] [INFO]: fid calculated: 2754.4528883256467
[2024-01-15 22:48:01,221] [INFO]: Finished evaluating epoch 1
[2024-01-15 22:48:01,221] [INFO]: starting calculating average current window
[2024-01-15 22:48:01,221] [INFO]: starting calculating average prior window
[2024-01-15 22:48:01,221] [INFO]: Epoch: 1, IS Mean: 1.00000021579094, IS Std: 1.3029850280554322e-07, FID: 2754.4528883256467, FID per class: {'airplane': 2382.538447479722, 'automobile': 2662.920649890014, 'bird': 2665.4007740944307, 'cat': 2817.5145248147855, 'deer': 3155.4593053215467, 'dog': 2656.931663999826, 'frog': 3002.942531544444, 'horse': 3009.109202177886, 'ship': 2670.7179591839376, 'truck': 2598.0070181402057}, Should Save Model: True, Avg Current Window: 3.0, Avg Prior Window: 8.0
[2024-01-15 22:48:01,239] [INFO]: Starting epoch 2
[2024-01-15 22:48:01,239] [INFO]: Loading CIFAR10 dataset with batch size 32 and train split
[2024-01-15 22:48:01,239] [INFO]: Using data augmentation
[2024-01-15 22:48:01,239] [INFO]: Normalizing images
[2024-01-15 22:48:29,180] [INFO]: Epoch [2/200]. Time taken for epoch 27 seconds
[2024-01-15 22:48:29,180] [INFO]: Evaluating epoch 2
[2024-01-15 22:48:29,180] [INFO]: Generating 100 images per class
[2024-01-15 22:48:29,187] [INFO]: Finished generating 100 images per class
[2024-01-15 22:48:29,254] [INFO]: Calculating Inception Score
[2024-01-15 22:48:30,555] [INFO]: Extracting features
[2024-01-15 22:48:30,555] [INFO]: using device: cuda
[2024-01-15 22:48:31,856] [INFO]: Calculating FID
[2024-01-15 22:48:32,451] [INFO]: fid calculated: 2372.22313766925
[2024-01-15 22:48:32,451] [INFO]: Calculating FID
[2024-01-15 22:48:32,988] [INFO]: fid calculated: 2643.333519212096
[2024-01-15 22:48:32,988] [INFO]: Calculating FID
[2024-01-15 22:48:33,515] [INFO]: fid calculated: 2641.4166738628237
[2024-01-15 22:48:33,515] [INFO]: Calculating FID
[2024-01-15 22:48:34,049] [INFO]: fid calculated: 2792.7505515602047
[2024-01-15 22:48:34,049] [INFO]: Calculating FID
[2024-01-15 22:48:34,590] [INFO]: fid calculated: 3131.3959927016185
[2024-01-15 22:48:34,590] [INFO]: Calculating FID
[2024-01-15 22:48:35,153] [INFO]: fid calculated: 2627.7154913179957
[2024-01-15 22:48:35,153] [INFO]: Calculating FID
[2024-01-15 22:48:35,680] [INFO]: fid calculated: 2985.5239524708327
[2024-01-15 22:48:35,680] [INFO]: Calculating FID
[2024-01-15 22:48:36,213] [INFO]: fid calculated: 2987.1466622245002
[2024-01-15 22:48:36,213] [INFO]: Calculating FID
[2024-01-15 22:48:36,727] [INFO]: fid calculated: 2652.607260222124
[2024-01-15 22:48:36,727] [INFO]: Calculating FID
[2024-01-15 22:48:37,269] [INFO]: fid calculated: 2581.916526255566
[2024-01-15 22:48:37,269] [INFO]: Calculating FID
[2024-01-15 22:48:37,781] [INFO]: fid calculated: 2733.922986545795
[2024-01-15 22:48:37,781] [INFO]: Finished evaluating epoch 2
[2024-01-15 22:48:37,781] [INFO]: starting calculating average current window
[2024-01-15 22:48:37,781] [INFO]: starting calculating average prior window
[2024-01-15 22:48:37,781] [INFO]: Epoch: 2, IS Mean: 1.0000000077758942, IS Std: 2.2388533890315373e-09, FID: 2733.922986545795, FID per class: {'airplane': 2372.22313766925, 'automobile': 2643.333519212096, 'bird': 2641.4166738628237, 'cat': 2792.7505515602047, 'deer': 3131.3959927016185, 'dog': 2627.7154913179957, 'frog': 2985.5239524708327, 'horse': 2987.1466622245002, 'ship': 2652.607260222124, 'truck': 2581.916526255566}, Should Save Model: True, Avg Current Window: 1379.2264441628233, Avg Prior Window: 2761.4528883256467
[2024-01-15 22:48:37,781] [INFO]: Starting epoch 3
[2024-01-15 22:48:37,781] [INFO]: Loading CIFAR10 dataset with batch size 32 and train split
[2024-01-15 22:48:37,781] [INFO]: Using data augmentation
[2024-01-15 22:48:37,782] [INFO]: Normalizing images
slurmstepd: error: *** JOB 23052516 ON uc2n520 CANCELLED AT 2024-01-15T22:48:58 ***
