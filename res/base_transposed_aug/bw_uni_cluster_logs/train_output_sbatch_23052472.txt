Setting up environment...
Mon Jan 15 22:41:45 2024       
+---------------------------------------------------------------------------------------+
| NVIDIA-SMI 535.104.12             Driver Version: 535.104.12   CUDA Version: 12.2     |
|-----------------------------------------+----------------------+----------------------+
| GPU  Name                 Persistence-M | Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp   Perf          Pwr:Usage/Cap |         Memory-Usage | GPU-Util  Compute M. |
|                                         |                      |               MIG M. |
|=========================================+======================+======================|
|   0  Tesla V100-SXM2-32GB           On  | 00000000:3A:00.0 Off |                    0 |
| N/A   37C    P0              40W / 300W |      9MiB / 32768MiB |      0%      Default |
|                                         |                      |                  N/A |
+-----------------------------------------+----------------------+----------------------+
                                                                                         
+---------------------------------------------------------------------------------------+
| Processes:                                                                            |
|  GPU   GI   CI        PID   Type   Process name                            GPU Memory |
|        ID   ID                                                             Usage      |
|=======================================================================================|
|    0   N/A  N/A      2065      G   /usr/libexec/Xorg                             8MiB |
+---------------------------------------------------------------------------------------+
Active conda environment: cuda
PyTorch version:  2.1.2+cu121
Is CUDA available: True
Number of CUDA Devices: 1
CUDA Device 0:  Tesla V100-SXM2-32GB
Current CUDA Device: 0
Current CUDA Device Name: Tesla V100-SXM2-32GB
The following modules are loaded:
Changed directory to /home/kit/stud/ubppd/pnn.
Running training ...

============================= JOB FEEDBACK =============================

NodeName=uc2n520
Job ID: 23052472
Cluster: uc2
User/Group: ubppd/stud
State: FAILED (exit code 1)
Nodes: 1
Cores per node: 2
CPU Utilized: 00:00:02
CPU Efficiency: 3.23% of 00:01:02 core-walltime
Job Wall-clock time: 00:00:31
Memory Utilized: 2.73 MB
Memory Efficiency: 0.00% of 195.31 GB
