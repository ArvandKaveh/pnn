Setting up environment...
Tue Jan 16 17:24:28 2024       
+---------------------------------------------------------------------------------------+
| NVIDIA-SMI 535.104.12             Driver Version: 535.104.12   CUDA Version: 12.2     |
|-----------------------------------------+----------------------+----------------------+
| GPU  Name                 Persistence-M | Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp   Perf          Pwr:Usage/Cap |         Memory-Usage | GPU-Util  Compute M. |
|                                         |                      |               MIG M. |
|=========================================+======================+======================|
|   0  Tesla V100-SXM2-32GB           On  | 00000000:3A:00.0 Off |                    0 |
| N/A   39C    P0              41W / 300W |      9MiB / 32768MiB |      0%      Default |
|                                         |                      |                  N/A |
+-----------------------------------------+----------------------+----------------------+
                                                                                         
+---------------------------------------------------------------------------------------+
| Processes:                                                                            |
|  GPU   GI   CI        PID   Type   Process name                            GPU Memory |
|        ID   ID                                                             Usage      |
|=======================================================================================|
|    0   N/A  N/A      2988      G   /usr/libexec/Xorg                             8MiB |
+---------------------------------------------------------------------------------------+
Active conda environment: cuda
PyTorch version:  2.1.2+cu121
Is CUDA available: True
Number of CUDA Devices: 1
CUDA Device 0:  Tesla V100-SXM2-32GB
Current CUDA Device: 0
Current CUDA Device Name: Tesla V100-SXM2-32GB
The following modules are loaded:
Changed directory to /home/kit/stud/ubppd/pnn.
Running training ...

============================= JOB FEEDBACK =============================

NodeName=uc2n513
Job ID: 23055001
Cluster: uc2
User/Group: ubppd/stud
State: CANCELLED (exit code 0)
Nodes: 1
Cores per node: 2
CPU Utilized: 00:00:03
CPU Efficiency: 0.03% of 02:39:18 core-walltime
Job Wall-clock time: 01:19:39
Memory Utilized: 18.70 GB
Memory Efficiency: 9.57% of 195.31 GB
