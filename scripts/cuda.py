import torch

try:
    print("PyTorch version: ", torch.__version__)
    print("Is CUDA available:", torch.cuda.is_available())
    print("Number of CUDA Devices:", torch.cuda.device_count())

    for i in range(torch.cuda.device_count()):
        print(f"CUDA Device {i}: ", torch.cuda.get_device_name(i))

    print("Current CUDA Device:", torch.cuda.current_device())
    print(
        "Current CUDA Device Name:",
        torch.cuda.get_device_name(torch.cuda.current_device()),
    )
except Exception as e:
    print(str(e))
