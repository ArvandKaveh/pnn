#!/bin/bash

#SBATCH --job-name=pnn_evaluate  # job name
#SBATCH --partition=gpu_4                  # mby GPU queue for the resource allocation.
#SBATCH --time=6:00:00                    # wall-clock time limit
#SBATCH --mem=200000                       # memory per node
#SBATCH --nodes=1                          # number of nodes to be used
#SBATCH --cpus-per-task=1                  # number of CPUs required per MPI task
#SBATCH --ntasks-per-node=1                # maximum count of tasks per node
#SBATCH --mail-type=ALL                    # Notify user by email when certain event types occur.
#SBATCH --mail-user=ubppd@student.kit.edu  # notification email address
#SBATCH --gres=gpu:1
#SBATCH --output=/home/kit/stud/ubppd/evals/train_output_sbatch_%j.txt
#SBATCH --error=/home/kit/stud/ubppd/evals/train_error_sbatch_%j.txt

# Set CWD to the directory of the project
export CWD="/home/kit/stud/ubppd/pnn"
export RES_DIR="/home/kit/stud/ubppd/evals"
mkdir -p $RES_DIR


# call ./setup_bw_uni_cluster.sh
source $CWD/scripts/setup_bw_uni_cluster.sh

# Change directory to CWD
cd $CWD || { echo "Could not change directory to $CWD. Exiting script ..."; exit 1;}
echo "Changed directory to $CWD."

# Run evaluation
echo "Running evaluation ..."
python -m src.evaluation.evaluation
