#!/bin/bash

#SBATCH --job-name=Test_Cuda          # job name
#SBATCH --partition=gpu_4                  # mby GPU queue for the resource allocation.
#SBATCH --time=00:10:00                    # wall-clock time limit
#SBATCH --mem=10000                       # memory per node
#SBATCH --nodes=1                          # number of nodes to be used
#SBATCH --cpus-per-task=1                  # number of CPUs required per MPI task
#SBATCH --ntasks-per-node=1                # maximum count of tasks per node
#SBATCH --mail-type=ALL                    # Notify user by email when certain event types occur.
#SBATCH --mail-user=ubppd@student.kit.edu  # notification email address
#SBATCH --gres=gpu:2
#SBATCH --output=/home/kit/stud/ubppd/cuda/train_output_sbatch_%j.txt
#SBATCH --error=/home/kit/stud/ubppd/cuda/train_error_sbatch_%j.txt

# Set CWD to the directory of the project
export CWD="/home/kit/stud/ubppd/pnn"

# Change directory to CWD
cd $CWD/scripts || { echo "Could not change directory to $CWD. Exiting script ..."; exit 1;}
echo "Changed directory to $CWD."

# Run training
echo "Running cuda test ..."
python cuda.py

echo "Setting up environment..."

export PYDIR=.                      # Export path to directory containing Python script.

nvcc --version
nvidia-smi
# module purge                                    # Unload all currently loaded modules.
# module load compiler/gnu/10.2                   # Load required modules.
# module load devel/python/3.8.6_gnu_10.2
# module load mpi/openmpi/4.1
module load devel/cuda/11.6
nvcc --version
nvidia-smi

# Change directory to CWD
cd $CWD/scripts || { echo "Could not change directory to $CWD. Exiting script ..."; exit 1;}
echo "Changed directory to $CWD."

# Run training
echo "Running cuda test ..."
python cuda.py

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home//kit/stud/ubppd/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home//kit/stud/ubppd/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home//kit/stud/ubppd/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home//kit/stud/ubppd/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# conda initialize
source ~/miniconda3/bin/activate pnn
nvcc --version
nvidia-smi


# Change directory to CWD
cd $CWD/scripts || { echo "Could not change directory to $CWD. Exiting script ..."; exit 1;}
echo "Changed directory to $CWD."

# Run training
echo "Running cuda test ..."
python cuda.py

echo "Python environment activated."
echo "The following modules are loaded:"
module list

conda install --yes pytorch==1.13.1 torchvision==0.14.1 torchaudio==0.13.1 pytorch-cuda=11.6 -c pytorch -c nvidia


# Change directory to CWD
cd $CWD/scripts || { echo "Could not change directory to $CWD. Exiting script ..."; exit 1;}
echo "Changed directory to $CWD."

# Run training
echo "Running cuda test ..."
python cuda.py

# Change directory to CWD
cd $CWD/scripts || { echo "Could not change directory to $CWD. Exiting script ..."; exit 1;}
echo "Changed directory to $CWD."

# Run training
echo "Running cuda test ..."
python cuda.py

echo "OS"
uname -a

echo "Nvidia SMI"
nvcc --version
nividia-smi

module purge 
module load devel/cuda/11.7

python cuda.py

conda install --yes pytorch==2.0.1 torchvision==0.15.2 torchaudio==2.0.2 pytorch-cuda=11.7 -c pytorch -c nvidia

python.py
