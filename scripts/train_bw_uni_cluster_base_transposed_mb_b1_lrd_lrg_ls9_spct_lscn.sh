#!/bin/bash

#SBATCH --job-name=base_transposed_mb_lr                 # job name
#SBATCH --partition=gpu_4              # mby GPU queue for the resource allocation.
#SBATCH --time=05:00:00                    # wall-clock time limit
#SBATCH --mem=200000                       # memory per node
#SBATCH --nodes=1                          # number of nodes to be used
#SBATCH --cpus-per-task=1                  # number of CPUs required per MPI task
#SBATCH --ntasks-per-node=1                # maximum count of tasks per node
#SBATCH --mail-type=ALL                    # Notify user by email when certain event types occur.
#SBATCH --mail-user=ubppd@student.kit.edu  # notification email address
#SBATCH --gres=gpu:1
#SBATCH --output=/home/kit/stud/ubppd/base_transposed_mb_b1_lrd_ls9_spct_ls/logs/output_sbatch_%j.txt
#SBATCH --error=/home/kit/stud/ubppd/base_transposed_mb_b1_lrd_ls9_spct_ls/logs/error_sbatch_%j.txt

# Set CWD to the directory of the project
export CWD="/home/kit/stud/ubppd/pnn"
export RES_DIR="/home/kit/stud/ubppd/base_transposed_mb_b1_lrd_ls9_spct_ls"
mkdir -p $RES_DIR


# call ./setup_bw_uni_cluster.sh
source $CWD/scripts/setup_bw_uni_cluster.sh

# Change directory to CWD
cd $CWD || { echo "Could not change directory to $CWD. Exiting script ..."; exit 1;}
echo "Changed directory to $CWD."

# Set log level
#export LOGLEVEL="DEBUG"

# Run training
echo "Running training ..."
python -m src.run_training_base \
  --home_dir . \
  --num_epochs 200 \
  --is_running_eval true \
  --generators "gen128t" "gen512t"\
  --discriminator "disc-basic" \
  -mbd true \
  -b1 0.5 \
  -lrd true \
  -lsf 0.9 \
  --use_spectral_norm true \
  --criterion "mse"
