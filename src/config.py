from dataclasses import dataclass, asdict
from pathlib import Path
from typing import List, Optional, Dict

from src.logger import get_logger
from src.models.criterion import Criterion

logger = get_logger("Config")


@dataclass
class Config:
    name: str
    base_path: Path
    batch_size: int
    noise_dim: int
    data_path: Optional[Path] = None
    log_path: Optional[Path] = None
    num_classes: int = 10
    num_epochs: int = 200
    evaluation_num_images_per_class: int = 100
    keep_best_models: int = 5
    keep_last_model = True
    start_evaluating_from_epoch: int = 0
    log_tensorboard: bool = False
    use_augmentation: bool = False
    label_smoothing_factor: float = 1.0

    # For Termination Criteria
    epsilon: float = 0.1
    avg_window_size: int = 10

    def __post_init__(self):
        # Save to $HOME/pnn/{generator_name}/
        self.data_path = self.base_path / self.name
        self.log_path = self.data_path / "evals"
        # Ensure that the log path and its parents directory exists
        self.log_path.mkdir(parents=True, exist_ok=True)

        if self.avg_window_size > self.num_epochs:
            raise ValueError(
                f"Average window size: {self.avg_window_size}"
                f"Number of epochs: {self.num_epochs}"
                "Window size cannot be bigger than num epochs"
                f"Consider setting a window size smaller than {self.num_epochs}"
            )


@dataclass
class CommandLineArgs:
    generators: List[str]
    discriminator: str
    home_dir: str
    criterion: Criterion
    learning_rate: float
    batch_size: int
    num_epochs: int
    noise_dim: int
    window_size: int
    is_running_eval: bool
    use_leaky_relu_discriminator: bool
    use_leaky_relu_generator: bool
    use_spectral_norm: bool
    use_augmentation: bool
    use_minibatch_discriminator: bool
    use_batch_normalisation_generator: bool
    label_smoothing_factor: float
    beta1: float
    beta2: float
    log_tensorboard: bool = False

    def __post_init__(self):
        if not 0.5 <= self.label_smoothing_factor <= 1.0:
            raise ValueError("use_label_smoothing must be between 0.5 and 1.0")


@dataclass
class EpochHistory:
    """History of training and evaluation within an epoch or iteration of training"""

    epoch: int
    # To keep track of the average score
    avg_current_window: float = 0.0
    avg_prior_window: float = 0.0
    inception_score_mean: float = 0.0
    inception_score_std: float = 0.0
    fid_per_class: Optional[Dict[str, float]] = None
    fid: float = 0.0
    should_save_model: bool = False
    model_parameters = None

    def __str__(self):
        return " | ".join(f"{k}: {v}" for k, v in asdict(self).items())

    def to_dict(self):
        # Convert dataclass to dictionary, excluding specific fields
        data = asdict(
            self,
            dict_factory=lambda x: {
                k: v for k, v in x if k not in ["should_save_model", "model_parameters"]
            },
        )
        return data


class TrainingHistory:
    def __init__(self, top_models_size: int):
        self.history: List[EpochHistory] = []
        self.keep_best_models = top_models_size

    def add(
        self,
        epoch_history: EpochHistory,
    ):
        epoch_history.should_save_model = True
        self.history.append(epoch_history)

    def get_history(self):
        return self.history

    def get_average(self, start: int, end: int):
        start = min(0, start)
        end = max(1, end)
        _sum = 0
        for epoch_history in self.history[start:end]:
            _sum += epoch_history.fid
        return _sum / end - start
