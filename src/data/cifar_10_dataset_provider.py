import os
import random
from pathlib import Path
from typing import List, Optional

import numpy as np
import torch
from matplotlib import pyplot as plt
from torch.utils.data import DataLoader
from torchvision import transforms, datasets

from src.logger import get_logger


class Cifar10DatasetProvider:
    def __init__(self, batch_size: int, shuffle: bool = True):
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.data_path = Path().parent / "datasets" / "cifar10"
        self.logger = get_logger("Cifar10DatasetProvider")

    def get_loader(
        self, is_train: bool = True, is_normalize: bool = True, is_augment: bool = False
    ):
        self.logger.debug(
            f"Loading CIFAR10 dataset with batch size {self.batch_size}"
            f" and {'train' if is_train else 'test'} split"
        )

        transform_parts = [transforms.ToTensor()]

        if is_train and is_augment:
            self.logger.debug("Using data augmentation")
            transform_parts.extend(
                [
                    transforms.RandomHorizontalFlip(),
                    transforms.RandomRotation(
                        degrees=random.choice([45, 90, 135, 180])
                    ),
                    transforms.ColorJitter(
                        brightness=0.2, contrast=0.2, saturation=0.2, hue=0.02
                    ),
                    transforms.RandomPerspective(distortion_scale=0.2, p=0.5),
                    transforms.RandomApply(
                        [transforms.GaussianBlur(kernel_size=(3, 3), sigma=(0.1, 2.0))],
                        p=0.2,
                    ),
                    transforms.Lambda(
                        lambda x: x + torch.randn_like(x) * 0.01
                    ),  # adding random noise
                ]
            )

        if is_normalize:
            self.logger.debug("Normalizing images")
            transform_parts.append(
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
            )

        transform = transforms.Compose(transform_parts)
        dataset = datasets.CIFAR10(
            root=str(self.data_path), transform=transform, train=is_train, download=True
        )
        return DataLoader(dataset, batch_size=self.batch_size, shuffle=self.shuffle)

    def get_real_images(self, num_images_per_class: int = 100):
        self.logger.debug(
            f"Loading {num_images_per_class} real images per class from CIFAR-10 dataset"
        )
        # Transformation for CIFAR-10 images
        transform = transforms.Compose(
            [
                transforms.Resize((299, 299)),  # Resize to match Inception v3 input
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
            ]
        )

        # Load CIFAR-10 test dataset
        cifar10_test = datasets.CIFAR10(
            root=str(self.data_path), train=False, download=True, transform=transform
        )

        # Split CIFAR-10 test dataset into classes
        real_images_per_class = [[] for _ in range(len(self.get_classes()))]
        for image, label in cifar10_test:
            real_images_per_class[label].append(image)

        # Flatten the list of lists to a single list of tensors
        flattened_images = [
            img
            for class_images in real_images_per_class
            for img in random.sample(class_images, num_images_per_class)
        ]

        self.logger.debug(
            f"Shape of a randomly selected real image: {flattened_images[0].shape}"
        )
        # Convert flattened list to a tensor of [num_classes * num_images_per_class, 3, 32, 32]
        return torch.stack(flattened_images)

    def preview(self, number_of_images: int = 1):
        loader = self.get_loader()
        # Take a peek at the data
        data_iter = iter(loader)  # Create an iterator from the DataLoader
        images, labels = next(data_iter)  # Fetch the first batch of data

        # Printing the shape of the images and labels tensors
        # `images.shape` should be [batch_size, channels, height, width]
        # `labels.shape` should be [batch_size]
        self.logger.debug(f"Shape of images: {images.shape}")
        self.logger.debug(f"Shape of labels: {labels.shape}")
        classes = self.get_classes()
        self.logger.debug(f"CIFAR-10 class names: {str(classes)}")

        # Function to denormalize and display an image
        def plot_image(image):
            image = image / 2 + 0.5  # denormalize
            plt.imshow(np.transpose(image.numpy(), (1, 2, 0)))
            plt.show()

        # Display the first image and its label
        for i in range(number_of_images):
            plot_image(images[i])
            self.logger.info(f"Label: {classes[labels[i]]}")

    @staticmethod
    def get_classes() -> List[str]:
        return [
            "airplane",
            "automobile",
            "bird",
            "cat",
            "deer",
            "dog",
            "frog",
            "horse",
            "ship",
            "truck",
        ]

    @staticmethod
    def num_classes() -> int:
        return len(Cifar10DatasetProvider.get_classes())


def plot_sample_grid(
    data_loader,
    num_images: int = 20,
    title: str = "Sample Images",
    save_dir: Optional[Path] = None,
):
    # Fetch a batch of images and labels
    images, labels = next(iter(data_loader))

    # Number of images in a row
    num_rows = np.ceil(num_images / 10).astype(int)

    # Set up the figure size
    plt.figure(figsize=(15, num_rows * 2 + 5))
    plt.title(title)

    for i in range(num_images):
        plt.subplot(num_rows, 10, i + 1)
        img = images[i] / 2 + 0.5  # De-normalize the image
        img = np.transpose(img.numpy(), (1, 2, 0))  # Convert from Tensor image
        plt.imshow(img)
        plt.axis("off")

    # plt.tight_layout()
    if save_dir is not None:
        save_dir = save_dir / "samples"
        os.makedirs(save_dir, exist_ok=True)
        plt.savefig(save_dir / f"{title}.png")
    plt.show()


if __name__ == "__main__":
    batch_size = 64  # Define your batch size
    cifar_provider = Cifar10DatasetProvider(batch_size=batch_size)
    data_loader = cifar_provider.get_loader(
        is_train=True, is_normalize=True, is_augment=True
    )

    plot_sample_grid(
        data_loader,
        num_images=60,
        title="CIFAR-10 Sample Augmented Images",
        save_dir=Path("/Users/arvandkaveh/Projects/kit/pnn/datasets"),
    )
