import json
from pathlib import Path

import torch

from src.config import EpochHistory
from src.data.cifar_10_dataset_provider import Cifar10DatasetProvider
from src.evaluation.fid_calculator import FidCalculator
from src.evaluation.inception_score import (
    transform_for_inception_score,
    inception_score,
)
from src.logger import get_logger
from src.models.generator import Generator512D02L5, Generator128D02L4

logger = get_logger("Evaluation")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def generate_images(generator, num_images_per_class):
    logger.debug("Generating %d images per class", num_images_per_class)
    generator.eval()
    generated_images = []
    for class_ in range(10):
        labels = torch.full(
            (num_images_per_class,), class_, dtype=torch.long, device=device
        )
        noise = torch.randn(num_images_per_class, 100, 1, 1, device=device)
        with torch.no_grad():
            images = generator(noise, labels).to(device)
        generated_images.append(images)
    logger.debug("Finished generating %d images per class", num_images_per_class)
    return torch.cat(generated_images, dim=0)


def evaluate_inception_score(images: torch.Tensor):
    logger.debug("Calculating inception score for %d images", images.size(0))
    return inception_score(images, device=device, batch_size=32)


def evaluate(generator, fid_calculator, epoch_num, epoch_history, dataset_provider):
    logger.debug("Evaluating epoch %d", epoch_num)
    generator.eval()
    generated_images = transform_for_inception_score(
        images=generate_images(generator=generator, num_images_per_class=100),
        device=device,
    )

    logger.debug("generated_images.shape: %s", generated_images.shape)
    logger.debug("generate images type: %s", type(generated_images))

    (
        epoch_history.inception_score_mean,
        epoch_history.inception_score_std,
    ) = evaluate_inception_score(generated_images)

    fid_calculator.set_generated_images(
        generated_images=generated_images, num_classes=dataset_provider.num_classes()
    )

    epoch_history.fid_per_class = fid_calculator.get_fid_score_per_class()
    epoch_history.fid = fid_calculator.get_fid_scores()

    logger.debug("Finished evaluating epoch %d", epoch_num)
    return epoch_history


def main():
    generator_model_pair = {
        Generator128D02L4: [
            Path(
                "/home/kit/stud/ubppd/res/pnn_results_base/pnn/results/Generator128D02L4/models"
            ),
            Path(
                "/home/kit/stud/ubppd/res/pnn_results_LRelu/pnn/results/Generator128D02L4/models"
            ),
            Path(
                "/home/kit/stud/ubppd/res/pnn_results_spectral/pnn/results/Generator128D02L4/models"
            ),
        ],
        Generator512D02L5: [
            Path(
                "/home/kit/stud/ubppd/res/pnn_results_base/pnn/results/Generator512D02L5/models"
            ),
            Path(
                "/home/kit/stud/ubppd/res/pnn_results_LRelu/pnn/results/Generator512D02L5/models"
            ),
            Path(
                "/home/kit/stud/ubppd/res/pnn_results_spectral/pnn/results/Generator512D02L5/models"
            ),
        ],
    }
    logger.debug("Starting the evaluation process")
    dataset_provider = Cifar10DatasetProvider(batch_size=32)
    fid_calculator = FidCalculator(
        data_provider=dataset_provider, num_images_per_class=100, device=device
    )

    for gen, list_of_paths in generator_model_pair.items():
        generator = gen(noise_dim=100).to(device)
        logger.debug("Evaluating %s", generator.NAME)
        for model_path in list_of_paths:
            epoch_histories = []
            for i in range(1, 201):
                file_name = f"generator_{i:03}"
                generator.load_state_dict(torch.load(model_path / file_name))

                # Evaluate the model sofar
                epoch_history = EpochHistory(epoch=i)
                epoch_histories.append(
                    evaluate(
                        generator=generator,
                        fid_calculator=fid_calculator,
                        epoch_num=i,
                        epoch_history=epoch_history,
                        dataset_provider=dataset_provider,
                    )
                )

            for epoch_history in epoch_histories:
                # Save epoch history
                history_folder = model_path.parent / "evals"
                history_folder.mkdir(parents=True, exist_ok=True)
                history_file = (
                    history_folder / f"epoch_history_{epoch_history.epoch:03}.json"
                )
                with history_file.open("w") as f:
                    json.dump(epoch_history.to_dict(), f, indent=4)


if __name__ == "__main__":
    main()
