import numpy as np
import torch
from scipy.linalg import sqrtm
from torchvision.models import inception_v3, Inception_V3_Weights

from src.data.cifar_10_dataset_provider import Cifar10DatasetProvider
from src.logger import get_logger

logger = get_logger("FID-Calculator")

from collections import namedtuple

inception_model = inception_v3(
    weights=Inception_V3_Weights.IMAGENET1K_V1, transform_input=False, aux_logits=True
)
# Define the named tuple
Statistics = namedtuple("Statistics", ["mean", "covariance"])


def get_statistics(activations):
    mu = activations.mean(axis=0)
    sigma = np.cov(activations, rowvar=False)
    # Add a small value to the diagonal for numerical stability
    sigma += np.eye(sigma.shape[0]) * 1e-6
    return Statistics(mean=mu, covariance=sigma)


def get_features(images, model=inception_model):
    """
    Extract feature vectors from a batch of images using a given model.

    This function processes a batch of images using a convolutional neural network (CNN)
    model and extracts feature vectors from these images. The images are processed in
    smaller batches of size `batch_size` to accommodate memory constraints.

    Parameters:
    - model (torch.nn.Module): The CNN model to be used for feature extraction.
      This model should take an image tensor as input and return feature vectors.
    - images (torch.Tensor): A 4D tensor containing the images. The tensor should have
      the shape (N, C, H, W), where N is the number of images, C is the number of channels,
      H is the height, and W is the width of the images.
    - batch_size (int, optional): The number of images to process in each batch.
      Defaults to 32.

    Returns:
    - numpy.ndarray: An array of the extracted feature vectors from the input images.
      The array shape will be (N, D), where N is the number of images and D is the
      dimensionality of the feature vectors.

    Note:
    - The function expects the input images to be preprocessed (normalized, resized, etc.)
      as required by the given `model`.
    - `device` should be set globally or within the function to specify whether to use CPU or GPU.
    """
    logger.debug("Extracting features")
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    logger.debug("using device: {}".format(device))
    model.to(device)
    model.eval()

    # Initialize an empty list to store features from each batch
    all_features = []

    for i in range(0, images.size(0), 32):
        batch = images[i : i + 32].to(device)
        with torch.no_grad():
            batch_features = model(batch)
            all_features.append(batch_features)

    # Concatenate all features into a single tensor
    all_features = torch.cat(all_features, dim=0)

    return all_features.cpu().numpy()


class FidCalculator:
    def __init__(
        self, data_provider: Cifar10DatasetProvider, num_images_per_class: int, device
    ):
        self.real_images_per_class_statistics = None
        self.real_images_statistics = None
        self.generated_images_per_class_statistics = None
        self.generated_images_statistics = None
        self.data_provider = data_provider
        self.real_images = self.data_provider.get_real_images(num_images_per_class).to(
            device
        )
        self.populate_stats_real_images(
            self.real_images, self.data_provider.num_classes()
        )

    def _calculate_statistics(self, images, num_classes):
        activations = get_features(images)
        activations_split = np.split(activations, num_classes, axis=0)

        overall_statistics = get_statistics(activations)
        per_class_statistics = [
            get_statistics(activations_split[i]) for i in range(num_classes)
        ]

        return overall_statistics, per_class_statistics

    def populate_stats_real_images(self, images, num_classes):
        (
            self.real_images_statistics,
            self.real_images_per_class_statistics,
        ) = self._calculate_statistics(images, num_classes)

    def set_generated_images(self, generated_images, num_classes):
        (
            self.generated_images_statistics,
            self.generated_images_per_class_statistics,
        ) = self._calculate_statistics(generated_images, num_classes)

    def get_fid_scores(self, class_index=None):
        # get mean and covariance real images
        if class_index is None:
            real_images_stats = self.real_images_statistics
            generated_images_stats = self.generated_images_statistics
        else:
            real_images_stats = self.real_images_per_class_statistics[class_index]
            generated_images_stats = self.generated_images_per_class_statistics[
                class_index
            ]

        return self._calculate_fid_distance(
            mu1=generated_images_stats.mean,
            sigma1=generated_images_stats.covariance,
            mu2=real_images_stats.mean,
            sigma2=real_images_stats.covariance,
        )

    def get_fid_score_per_class(self):
        results = {}
        for class_index, class_name in enumerate(self.data_provider.get_classes()):
            # Compute FID
            results[class_name] = self.get_fid_scores(class_index=class_index)
        return results

    def _calculate_fid_distance(self, mu1, sigma1, mu2, sigma2) -> float:
        """Calculates the Fréchet Inception Distance (FID) between two sets of activations."""
        logger.debug("Calculating FID")

        # Calculate sum squared difference between means
        ssdiff = np.sum((mu1 - mu2) ** 2.0)

        # Calculate sqrt of product between cov
        covmean = sqrtm(sigma1.dot(sigma2))

        # Numerical errors might give a slight  imaginary component
        if np.iscomplexobj(covmean):
            covmean = covmean.real

        fid = ssdiff + np.trace(sigma1 + sigma2 - 2.0 * covmean)
        logger.debug(f"fid calculated: {fid}")
        return fid
