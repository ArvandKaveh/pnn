import numpy as np
import torch
from scipy.stats import entropy
from torch.nn.functional import softmax
from torchvision import transforms
from torchvision.models import inception_v3, Inception_V3_Weights

from src.logger import get_logger

logger = get_logger("InceptionScore")

inception_model = inception_v3(
    weights=Inception_V3_Weights.IMAGENET1K_V1, transform_input=False, aux_logits=True
)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
inception_model.to(device)
inception_model.eval()


def inception_score(images, device, batch_size=32, resize=True, splits=10):
    """
    Computes the inception score of the generated images.

    :param images: Torch dataset of (3xHxW) numpy images normalized in the range [-1, 1]
    :device: The device to
    :param batch_size: Batch size for feeding into Inception v3
    :param resize: Resize to 299x299 before feeding into Inception
    :param splits: Number of splits
    :return: Mean and standard deviation of the Inception Score
    """
    logger.debug("Calculating Inception Score")

    if resize:
        images = transform_for_inception_score(images, device)

    up = torch.nn.Upsample(size=(299, 299), mode="bilinear").to(device)

    def get_predictions(x):
        if resize:
            x = up(x)
        x = inception_model(x)
        return softmax(x, dim=1).data.cpu().numpy()

    # Compute the predictions for all images
    predictions = np.zeros((len(images), 1000))

    for i in range(0, len(images), batch_size):
        # Extract the batch of images directly without converting from NumPy
        batch = images[i : i + batch_size].to(device)
        predictions[i : i + batch_size] = get_predictions(batch)

    # Now compute the mean kl-div
    split_scores = []

    for k in range(splits):
        part = predictions[
            k * (len(images) // splits) : (k + 1) * (len(images) // splits), :
        ]
        py = np.mean(part, axis=0)
        scores = []
        for i in range(part.shape[0]):
            pyx = part[i, :]
            scores.append(entropy(pyx, py))
        split_scores.append(np.exp(np.mean(scores)))

    return np.mean(split_scores), np.std(split_scores)


def transform_for_inception_score(images, device):
    # Transform for resizing and normalizing images for Inception v3
    transform = transforms.Compose(
        [
            transforms.Resize((299, 299)),  # Resize to 299x299
            transforms.Lambda(lambda x: (x + 1) / 2),  # Rescale from [-1, 1] to [0, 1]
        ]
    )

    # Apply transformations to the entire batch
    transformed_images = torch.stack([transform(img) for img in images])
    return transformed_images.to(
        device
    )  # Move transformed images to the specified device
