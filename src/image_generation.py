import torch
import torchvision
import torchvision.utils as utils

from data.cifar_10_dataset_provider import Cifar10DatasetProvider
from results_utils import (
    AVAILABLE_RESULTS,
    Res,
    get_top_n_per_kpi,
    read_data,
    get_checkpoint,
)
from src.logger import get_logger
from utils import average_checkpoints

logger = get_logger("ImageGeneration")


def load_generator_for_eval(
    generator,
    model_params_to_average_from,
    device,
):
    generator.load_state_dict(
        torch.load(
            average_checkpoints(model_params_to_average_from),
            map_location=torch.device(device),
        )
    )
    generator.eval()
    return generator


# Function to generate images
def generate_images(generator, num_images, num_classes, z_dim, device):
    generated_images = []
    for class_ in range(num_classes):
        labels = torch.full((num_images,), class_, dtype=torch.long, device=device)
        noise = torch.randn(num_images, z_dim, 1, 1, device=device)
        with torch.no_grad():
            images = generator(noise, labels).cpu()
        generated_images.append(images)
    return torch.cat(generated_images, dim=0)


def generate_best_images_per_class(
    result: Res,
    top_n: int,
    num_images: int,
    classes: list[str],
    z_dim: int,
    end: int,
    device,
    use_grid: bool = False,
):
    logger.info(
        f"-----------------------------------------------------------------------------------{result.name}:"
    )
    logger.info(
        f"Generating images for {num_images} for the model {result.root} using top {top_n} checkpoints"
    )
    generated_images = []
    generator = result.generator(
        noise_dim=result.noise_dim, use_leaky_relu=result.use_leaky_relu
    ).to(device)

    top_n_kpi = get_top_n_per_kpi(
        read_data(res.root / "evals", start=1, end=end), top_n
    )
    for i, class_ in enumerate(classes):
        top_n_models = top_n_kpi.get(f"fid_per_class_{class_}")
        logger.info(f"- Generating images for class {class_}...")
        logger.info(
            f"\t Using the the average checkpoint from the checkpoints: {top_n_models}"
        )
        best_n_checkpoints = [
            get_checkpoint(result.root / "models", checkpoint)
            for checkpoint, _ in top_n_models
        ]
        generator = load_generator_for_eval(
            generator=generator,
            model_params_to_average_from=best_n_checkpoints,
            device=device,
        )

        labels = torch.full((num_images,), i, dtype=torch.long, device=device)
        noise = torch.randn(num_images, z_dim, 1, 1, device=device)
        with torch.no_grad():
            images = generator(noise, labels).cpu()
        generated_images.append(images)
    logger.info(
        "-----------------------------------------------------------------------------------"
    )
    concatenated_generated_images = torch.cat(generated_images, dim=0)
    if use_grid:
        return torchvision.utils.make_grid(
            concatenated_generated_images, normalize=True
        )
    return concatenated_generated_images


def generate_best_images_fid_overall(
    result: Res,
    top_n: int,
    num_images: int,
    classes: list[str],
    z_dim: int,
    end: int,
    device,
    use_grid: bool = False,
):
    logger.info(
        f"-----------------------------------------------------------------------------------{result.name}:"
    )
    logger.info(
        f"Generating images for {num_images} for the model {result.root} using top {top_n} checkpoints"
    )
    generated_images = []
    generator = result.generator(
        noise_dim=result.noise_dim, use_leaky_relu=result.use_leaky_relu
    ).to(device)

    top_n_kpi = get_top_n_per_kpi(
        read_data(res.root / "evals", start=1, end=end), top_n
    )
    top_n_models = top_n_kpi.get("fid")
    best_n_checkpoints = [
        get_checkpoint(result.root / "models", checkpoint)
        for checkpoint, _ in top_n_models
    ]
    generator = load_generator_for_eval(
        generator=generator,
        model_params_to_average_from=best_n_checkpoints,
        device=device,
    )
    for i, class_ in enumerate(classes):
        logger.info(f"- Generating images for class {class_}...")
        logger.info(
            f"\t Using the the average checkpoint from the checkpoints: {top_n_models}"
        )

        labels = torch.full((num_images,), i, dtype=torch.long, device=device)
        noise = torch.randn(num_images, z_dim, 1, 1, device=device)
        with torch.no_grad():
            images = generator(noise, labels).cpu()
        generated_images.append(images)
    logger.info(
        "-----------------------------------------------------------------------------------"
    )
    concatenated_generated_images = torch.cat(generated_images, dim=0)
    if use_grid:
        return torchvision.utils.make_grid(
            concatenated_generated_images, normalize=True
        )
    return concatenated_generated_images


# Function to save images
def save_images(images, base_dir, model_file_name, class_names):
    for i, class_label in enumerate(class_names):
        # Create directory for each class
        class_dir = base_dir / model_file_name / class_label
        class_dir.mkdir(parents=True, exist_ok=True)

        # Save images for each class
        for j in range(images.shape[0] // len(class_names)):
            # Image index for the current class
            img_index = i * (images.shape[0] // len(class_names)) + j
            img = images[img_index]

            # Filename for the image
            filename = f"img{j:03d}.png"
            filepath = class_dir / filename

            # Save image
            utils.save_image(img, str(filepath))


def save_grid_image(image, base_dir, model_file_name):
    grid_dir = base_dir
    grid_dir.mkdir(parents=True, exist_ok=True)
    filepath = grid_dir / model_file_name
    utils.save_image(image, str(filepath))


if __name__ == "__main__":
    device = "cuda" if torch.cuda.is_available() else "cpu"
    num_images_per_class = 8
    top_n = 3
    end = 200
    use_grid = True

    for res in AVAILABLE_RESULTS:
        synthesized_images = generate_best_images_fid_overall(
            result=res,
            top_n=top_n,
            end=end,
            num_images=num_images_per_class,
            classes=Cifar10DatasetProvider.get_classes(),
            z_dim=res.noise_dim,
            device=device,
            use_grid=use_grid,
        )

        if use_grid:
            save_grid_image(
                image=synthesized_images,
                base_dir=res.root / "grids_10x16",
                model_file_name=f"top-{top_n}_end-{end}_fid_overall.png",
            )
        else:
            # Save generated images
            save_images(
                images=synthesized_images,
                base_dir=res.root / "best_images_grid",
                model_file_name=f"img_top-{top_n}_end-{end}_fid_overall",
                class_names=Cifar10DatasetProvider.get_classes(),
            )
