import enum

from torch import nn


class Criterion(enum.Enum):
    """Enumeration of supported criterion types"""

    BCE_LOGITS = (
        "bce_logits",
        "Binary cross entropy loss with logits",
        nn.BCEWithLogitsLoss,
    )
    MSE = ("mse", "Mean squared error", nn.MSELoss)

    def __new__(cls, value: str, description: str, loss_function: nn.Module):
        obj = object.__new__(cls)
        obj._value_ = value
        obj.description = description
        obj.loss_function = loss_function
        return obj

    def __str__(self):
        return str({"short name": self.value, "description": self.description})


if __name__ == "__main__":
    a = Criterion("mse")
    print("%s %r", a, a)
    print(f"{a}")
