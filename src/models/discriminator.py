from typing import List, Final

from src.logger import get_logger

logger = get_logger("DiscriminatorClass")

import torch
import torch.nn as nn
import torch.nn.init as init


class MinibatchDiscrimination(nn.Module):
    def __init__(self, in_features, out_features, kernel_dims, mean=False):
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.kernel_dims = kernel_dims
        self.mean = mean
        self.T = nn.Parameter(torch.Tensor(in_features, out_features, kernel_dims))
        logger.debug(f"T: {self.T.shape}")
        init.normal_(self.T, 0, 1)

    def forward(self, x):
        # x is NxA
        # T is AxBxC
        logger.debug(f"input x: {x.shape}")
        batch_size = x.size(0)
        x = x.view(batch_size, -1)  # New line to flatten x
        logger.debug(f"x_2d: {x.shape}")
        logger.debug(f"T: {self.T.shape}")
        t_2d = self.T.view(self.in_features, -1)
        logger.debug(f"T_2d.shape: {t_2d.shape}")
        matrices = x.mm(t_2d)
        logger.debug(f"matrices (x_2d x T_2d): {matrices.shape}")
        matrices = matrices.view(-1, self.out_features, self.kernel_dims)
        logger.debug(f"matrices expanded: {matrices.shape}")

        M = matrices.unsqueeze(0)  # 1xNxBxC
        M_T = M.permute(1, 0, 2, 3)  # Nx1xBxC
        norm = torch.abs(M - M_T).sum(3)  # NxNxB
        expnorm = torch.exp(-norm)
        o_b = expnorm.sum(0) - 1  # NxB, subtract self distance
        if self.mean:
            o_b /= x.size(0) - 1

        logger.debug(f"before cat x: {x.shape}")
        logger.debug(f"before cat o_b: {o_b.shape}")
        x = torch.cat([x, o_b], 1)
        logger.debug(f"output x: {x.shape}")
        return x


class Discriminator(nn.Module):
    """
    Base class for a Discriminator in a GAN setup.
    """

    NAME: str = "Discriminator"
    SHORT_NAME: str = "disc"

    def __init__(self, num_classes: int, *args, **kwargs):
        """
        Initializes the Discriminator module.
        """
        super().__init__(*args, **kwargs)
        self.label_emb = nn.Embedding(num_classes, num_classes)
        self.model = None

    def forward(self, x, labels):
        # Embed class labels
        labels_emb = self.label_emb(labels)
        labels_emb = labels_emb.view(labels_emb.size(0), -1, 1, 1)

        # Concatenate label embeddings and image
        # Assuming labels_emb and x have same spatial dimensions
        x = torch.cat([x, labels_emb.expand(-1, -1, x.size(2), x.size(3))], 1)
        return self.model(x)


class DiscriminatorBasic(Discriminator):
    NAME: Final[str] = "DiscriminatorBasic"
    SHORT_NAME: Final[str] = "disc-basic"

    def __init__(
        self,
        num_classes=10,
        img_channels=3,
        hidden_dimension=64,
        use_spectral_norm=False,
        use_leaky_relu=False,
        use_minibatch_discriminator=False,
        # Minibatch Discrimination parameters
        mb_discrim_out_features=None,  # Set appropriate value
    ):
        super().__init__(num_classes=num_classes)

        def conv_block(
            in_channels,
            out_channels,
            kernel_size=4,
            stride=2,
            padding=1,
            use_norm: bool = True,
        ):
            bias = not use_norm
            layers = [
                nn.Conv2d(
                    in_channels, out_channels, kernel_size, stride, padding, bias=bias
                )
            ]
            if use_spectral_norm:
                layers[0] = nn.utils.spectral_norm(layers[0])
            if use_norm:
                layers.append(nn.BatchNorm2d(out_channels))
            layers.append(nn.LeakyReLU(0.1) if use_leaky_relu else nn.ReLU())
            layers.append(nn.Dropout(0.2))
            return layers

        conv_layers = []
        conv_layers.extend(
            conv_block(img_channels + num_classes, hidden_dimension, use_norm=True)
        )
        conv_layers.extend(conv_block(hidden_dimension, hidden_dimension * 2))
        conv_layers.extend(conv_block(hidden_dimension * 2, hidden_dimension * 4))

        if use_minibatch_discriminator:
            # Define minibatch discrimination layer
            # _Calculate the size of the flattened features after the conv layers
            flattened_size = hidden_dimension * 4 * 4 * 4

            self.minibatch_discrimination = MinibatchDiscrimination(
                in_features=flattened_size,
                out_features=mb_discrim_out_features,
                kernel_dims=5,
            )
            conv_layers.append(self.minibatch_discrimination)
            # Adjust the input size of the final linear layer
            final_linear_in_features = flattened_size + mb_discrim_out_features
            final_layers = [nn.Flatten(), nn.Linear(final_linear_in_features, 1)]
        else:
            final_layers = [
                nn.Conv2d(hidden_dimension * 4, 1, 4, 1, 0, bias=False),
                nn.Flatten(),
            ]

        self.model = nn.Sequential(*conv_layers, *final_layers)


discriminators: List[Discriminator] = [DiscriminatorBasic]


def get_discriminator(short_name: str) -> Discriminator:
    for disc in discriminators:
        if short_name == disc.SHORT_NAME:
            return disc
    raise ValueError(
        f"Unknown discriminator '{short_name}'. "
        f"The available discriminators are: {list(map(lambda x: x.SHORT_NAME, discriminators))}"
    )


def test_discriminator():
    N, in_channels, H, W = 32, 3, 32, 32
    num_classes = 10
    real_img = torch.randn(N, in_channels, H, W)
    labels = torch.randint(0, num_classes, (N,))

    # Instantiate a specific discriminator
    discriminator = DiscriminatorBasic(
        num_classes=num_classes, img_channels=in_channels
    )

    # Forward pass
    output = discriminator(real_img, labels)

    # Check output shape
    expected_shape = (N, 1)
    assert (
        output.shape == expected_shape
    ), f"Discriminator output shape should be {expected_shape}, but got {output.shape}"


def test_discriminator_mb():
    N, in_channels, H, W = 32, 3, 32, 32
    num_classes = 10
    real_img = torch.randn(N, in_channels, H, W)
    labels = torch.randint(0, num_classes, (N,))
    hidden_dimension = 64

    # Instantiate a specific discriminator
    discriminator = DiscriminatorBasic(
        num_classes=num_classes,
        img_channels=in_channels,
        hidden_dimension=hidden_dimension,
        use_minibatch_discriminator=True,
        mb_discrim_out_features=100,
    )
    print(discriminator.model)

    # Forward pass
    output = discriminator(real_img, labels)

    # Check output shape
    expected_shape = (N, 1)
    assert (
        output.shape == expected_shape
    ), f"Discriminator with Minibatch Discrimination output shape should be {expected_shape}, but got {output.shape}s "


if __name__ == "__main__":
    test_discriminator()
    test_discriminator_mb()
