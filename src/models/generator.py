from typing import List, Final

import torch
from torch import nn

from src.logger import get_logger

logger = get_logger("GeneratorClass")


class Generator(nn.Module):
    NAME: str = "Generator"
    SHORT_NAME: str = "gen"

    def __init__(self, class_dim, use_leaky_relu, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label_emb = nn.Embedding(class_dim, class_dim)
        self.model = None
        self.activation_function = nn.LeakyReLU(0.2) if use_leaky_relu else nn.ReLU()

    def forward(self, noise, labels):
        # Embed the labels and reshape them
        labels_emb = self.label_emb(labels)
        labels_emb = labels_emb.view(
            labels.size(0), -1
        )  # Flatten label embeddings to 2D

        # Ensure noise is a 2D tensor
        noise = noise.view(noise.size(0), -1)

        # Concatenate noise and label embeddings
        combined = torch.cat(
            [noise, labels_emb], dim=1
        )  # Ensure the size is [batch_size, latent_dim + class_dim]

        # Generate an image from the combined input
        img = self.model(combined)
        return img


class Generator128D02L4(Generator):
    NAME: Final[str] = "Generator128D02L4"
    SHORT_NAME: Final[str] = "gen128"

    def __init__(
        self,
        noise_dim,
        use_leaky_relu=False,
        use_batch_normalisation_generator: bool = False,
        class_dim=10,
        image_channels=3,
        hidden_dim=128,
    ):
        super(Generator128D02L4, self).__init__(
            class_dim=class_dim, use_leaky_relu=use_leaky_relu
        )

        # Initialize list to dynamically add layers
        layers = [nn.Linear(noise_dim + class_dim, hidden_dim * 8 * 8)]

        # Optionally add batch normalization
        if use_batch_normalisation_generator:
            layers.append(nn.BatchNorm1d(hidden_dim * 8 * 8))

        layers += [
            self.activation_function,
            nn.Dropout(0.2),
            nn.Unflatten(1, (hidden_dim, 8, 8)),
        ]

        # Define the sequence of Conv2d and Upsample layers
        conv_block_specs = [
            (hidden_dim, hidden_dim // 2),
            (hidden_dim // 2, hidden_dim // 4),
        ]

        for in_channels, out_channels in conv_block_specs:
            layers.append(
                nn.Conv2d(
                    in_channels,
                    out_channels,
                    kernel_size=3,
                    stride=1,
                    padding=1,
                    bias=not use_batch_normalisation_generator,
                )
            )
            if use_batch_normalisation_generator:
                layers.append(nn.BatchNorm2d(out_channels))
            layers += [
                self.activation_function,
                nn.Dropout(0.2),
                nn.Upsample(scale_factor=2),
            ]

        # Final Conv2d layer
        layers.append(
            nn.Conv2d(
                hidden_dim // 4, image_channels, kernel_size=3, stride=1, padding=1
            )
        )
        layers.append(nn.Tanh())

        self.model = nn.Sequential(*layers)


class Generator128D02L4Transposed(Generator):
    NAME: Final[str] = "Generator128D02L4Transpose"
    SHORT_NAME: Final[str] = "gen128t"

    def __init__(
        self,
        noise_dim,
        use_leaky_relu=False,
        use_batch_normalisation_generator: bool = False,
        class_dim=10,
        image_channels=3,
        hidden_dim=128,
    ):
        super(Generator128D02L4Transposed, self).__init__(
            class_dim=class_dim, use_leaky_relu=use_leaky_relu
        )
        # Start with an empty list to dynamically add layers
        layers = [
            nn.Linear(noise_dim + class_dim, hidden_dim * 8 * 8),
            self.activation_function,
            nn.Dropout(0.2),
            nn.Unflatten(1, (hidden_dim, 8, 8)),
        ]

        # Define transposed convolution blocks as tuples for easier management
        transposed_conv_blocks = [
            (hidden_dim, hidden_dim // 2, 4, 2, 1),
            (hidden_dim // 2, hidden_dim // 4, 4, 2, 1),
        ]

        # Add ConvTranspose2d and possible BatchNorm2d layers
        for (
            in_channels,
            out_channels,
            kernel_size,
            stride,
            padding,
        ) in transposed_conv_blocks:
            layers.append(
                nn.ConvTranspose2d(
                    in_channels,
                    out_channels,
                    kernel_size,
                    stride,
                    padding,
                    bias=not use_batch_normalisation_generator,
                )
            )
            if use_batch_normalisation_generator:
                layers.append(nn.BatchNorm2d(out_channels))
            layers.append(self.activation_function)
            layers.append(nn.Dropout(0.2))

        # Final Conv2d to produce the image
        layers.append(
            nn.Conv2d(
                hidden_dim // 4, image_channels, kernel_size=3, stride=1, padding=1
            )
        )
        layers.append(nn.Tanh())

        # Create the model
        self.model = nn.Sequential(*layers)


class Generator512D02L5(Generator):
    NAME: Final[str] = "Generator512D02L5"
    SHORT_NAME: Final[str] = "gen512"

    def __init__(
        self,
        noise_dim,
        use_leaky_relu=False,
        use_batch_normalisation_generator: bool = False,
        class_dim=10,
        image_channels=3,
        hidden_dim=512,
    ):
        super(Generator512D02L5, self).__init__(
            class_dim=class_dim, use_leaky_relu=use_leaky_relu
        )

        # Initialize list to dynamically add layers
        layers = [
            nn.Linear(noise_dim + class_dim, hidden_dim * 4 * 4),
            self.activation_function,
            nn.Dropout(0.2),
            nn.Unflatten(1, (hidden_dim, 4, 4)),
        ]

        # Define the sequence of layers with conditional batch normalization
        conv_block_specs = [
            (hidden_dim, hidden_dim // 2),
            (hidden_dim // 2, hidden_dim // 4),
            (hidden_dim // 4, hidden_dim // 8),
        ]

        for in_channels, out_channels in conv_block_specs:
            layers.append(
                nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=1, padding=1)
            )
            if use_batch_normalisation_generator:
                layers.append(nn.BatchNorm2d(out_channels))
            layers += [
                self.activation_function,
                nn.Dropout(0.2),
                nn.Upsample(scale_factor=2),
            ]

        # Final Conv2d layer to produce the output image
        layers.append(
            nn.Conv2d(
                hidden_dim // 8, image_channels, kernel_size=3, stride=1, padding=1
            )
        )
        layers.append(nn.Tanh())

        self.model = nn.Sequential(*layers)


class Generator512D02L5Transposed(Generator):
    NAME: Final[str] = "Generator512D02L5Transpose"
    SHORT_NAME: Final[str] = "gen512t"

    def __init__(
        self,
        noise_dim,
        use_leaky_relu=False,
        use_batch_normalisation_generator: bool = False,
        class_dim=10,
        image_channels=3,
        hidden_dim=512,
    ):
        super(Generator512D02L5Transposed, self).__init__(
            class_dim=class_dim, use_leaky_relu=use_leaky_relu
        )

        # Start constructing the model with a list
        layers = [
            nn.Linear(noise_dim + class_dim, hidden_dim * 4 * 4),
            self.activation_function,
            nn.Dropout(0.2),
            nn.Unflatten(1, (hidden_dim, 4, 4)),
        ]

        # Define the transposed convolution blocks
        transposed_conv_blocks = [
            (hidden_dim, hidden_dim // 2),
            (hidden_dim // 2, hidden_dim // 4),
            (hidden_dim // 4, hidden_dim // 8),
        ]

        for in_channels, out_channels in transposed_conv_blocks:
            layers.append(
                nn.ConvTranspose2d(
                    in_channels,
                    out_channels,
                    kernel_size=4,
                    stride=2,
                    padding=1,
                    bias=not use_batch_normalisation_generator,
                )
            )
            if use_batch_normalisation_generator:
                layers.append(nn.BatchNorm2d(out_channels))
            layers.append(self.activation_function)
            layers.append(nn.Dropout(0.2))

        # Final Conv2d layer to produce the image
        layers.append(
            nn.Conv2d(
                hidden_dim // 8, image_channels, kernel_size=3, stride=1, padding=1
            )
        )
        layers.append(nn.Tanh())

        # Create the sequential model
        self.model = nn.Sequential(*layers)


generators: List[Generator] = [
    Generator128D02L4,
    Generator128D02L4Transposed,
    Generator512D02L5,
    Generator512D02L5Transposed,
]


def get_generator(short_name: str) -> Generator:
    for gen in generators:
        if short_name == gen.SHORT_NAME:
            return gen
    raise ValueError(
        f"Unknown generator {short_name}. The available generators are: {list(map(lambda x: x.SHORT_NAME, generators))}"
    )


if __name__ == "__main__":
    gen = get_generator("gen512")
    g = gen(noise_dim=128)
    print(g.model)
