import argparse

from src.models.discriminator import discriminators
from src.models.generator import generators
from src.models.criterion import Criterion
from src.config import CommandLineArgs


def get_cli_args() -> CommandLineArgs:
    parser = argparse.ArgumentParser(description="Process some input.")
    parser.add_argument(
        "--home_dir",
        "-hd",
        type=str,
        required=True,
        help="The home directory of the project",
    )
    parser.add_argument(
        "--generators",
        "-gens",
        type=str,
        nargs="+",
        required=True,
        help="The short name of the space seperated generators. Available generators are:"
        f" {', '.join(gen.SHORT_NAME for gen in generators)}",
    )
    parser.add_argument(
        "--discriminator",
        "-disc",
        type=str,
        required=True,
        help="The short name of the discriminator. Available discriminators are:"
        f" {', '.join(gen.SHORT_NAME for gen in discriminators)}",
    )
    parser.add_argument(
        "--learning_rate", "-lr", type=float, default=0.00003, help="The learning rate"
    )
    parser.add_argument(
        "--batch_size", "-bs", type=int, default=32, help="The batch size"
    )
    parser.add_argument(
        "--num_epochs",
        "-ne",
        type=int,
        default=50,
        help="The number of epochs. Min value: 5",
    )
    parser.add_argument(
        "--noise_dim", "-nd", type=int, default=100, help="The noise dimension"
    )
    parser.add_argument(
        "--window_size",
        "-ws",
        type=int,
        default=5,
        help="The window size for averaging",
    )
    parser.add_argument(
        "--is_running_eval",
        "-ire",
        type=bool,
        default=True,
        help="Whether to run evaluation while training or not",
    )
    parser.add_argument("--use_spectral_norm", "-sn", type=bool, default=False)
    parser.add_argument(
        "--use_batch_normalisation_generator", "-bng", type=bool, default=False
    )
    parser.add_argument(
        "--use_leaky_relu_discriminator", "-lrd", type=bool, default=False
    )
    parser.add_argument("--use_leaky_relu_generator", "-lrg", type=bool, default=False)
    parser.add_argument("--use_augmentation", "-ua", type=bool, default=False)
    parser.add_argument("--log_tensorboard", "-tb", type=bool, default=False)
    parser.add_argument("--beta1", "-b1", type=float, default=0.9)
    parser.add_argument("--beta2", "-b2", type=bool, default=0.999)
    parser.add_argument(
        "--criterion",
        "-ce",
        type=Criterion,
        default="bce_logits",
        help=f"Available criteria are: {', '.join(str(cr) for cr in Criterion)}",
    )
    parser.add_argument("--label_smoothing_factor", "-lsf", type=float, default=1.0)
    parser.add_argument(
        "--use_minibatch_discriminator", "-mbd", type=bool, default=False
    )

    return CommandLineArgs(**vars(parser.parse_args()))
