import enum
import logging
import os
from dataclasses import dataclass
from typing import List

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

from results_utils import (
    AVAILABLE_RESULTS,
    filter_by_tags,
    Res,
    read_data,
)

plt.style.use("seaborn-v0_8-darkgrid")  # Use a professional plot style

# Define global colors and markers
COLORS = [
    "black",
    # "black",
    "red",
    "blue",
    "green",
    "purple",
    "orange",
    "cyan",
    "magenta",
    "yellow",
    "brown",
]
MARKERS = ["o", "x", "s", "*", "^", "D", "h", "p", ">", "<"]

#     '-' or 'solid': Solid line
#     '--' or 'dashed': Dashed line
#     '-.' or 'dashdot': Dash-dot line
#     ':' or 'dotted': Dotted line
LINESTYLES = [
    "-",
    "--",
    "-",
    "--",
    "-",
    "--",
    "-",
    "--",
    "-",
    "--",
    "-",
    "--",
]


class Metric(enum.Enum):
    FID_PER_CLASS = "fid_per_class"
    FID = "fid"
    INCEPTION_MEAN = "inception_mean"
    INCEPTION_STD = "inception_std"


@dataclass
class PlotData:
    epoch: list
    fid: list
    inception_mean: list
    inception_std: list
    fids_per_class: dict
    label: str


def plot_fid_per_class(
    scores: List[PlotData],
    save_path,
):
    for idx, k in enumerate(scores[0].fids_per_class.keys()):
        plt.figure(figsize=(10, 6))
        ax = plt.gca()  # Get current axis

        # [existing plot code]

        # Enable and customize the spines for the frame
        for spine in ax.spines.values():
            spine.set_visible(True)
            spine.set_color("black")
            spine.set_linewidth(1)

        for i, metric in enumerate(scores):
            plt.plot(
                metric.epoch,
                metric.fids_per_class[k],
                color=COLORS[i % len(COLORS)],
                marker=MARKERS[i % len(MARKERS)],
                linestyle=LINESTYLES[i % len(LINESTYLES)],
                label=f"{metric.label} - {k}",
                markersize=4,
            )

        plt.title(f"FID per Class {k} over Epochs", fontsize=14)
        plt.xlabel("Epoch", fontsize=12)
        plt.ylabel("FID", fontsize=12)
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)
        plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
        legend = plt.legend(
            frameon=True,
            facecolor="white",
            edgecolor="black",
            shadow=True,
            framealpha=1.0,
        )
        legend.set_zorder(3)
        plt.grid(True, linestyle="--", color="gray", linewidth=0.5, alpha=0.7)
        plt.tight_layout()

        # Save each plot with a unique name
        plt.savefig(
            f"{save_path}_class_{k}.png", format="png", dpi=300
        )  # Save as high-quality PNG
        plt.show()
        plt.close()


def plot_fid_per_class_singular(
    scores: List[PlotData],
    save_path,
):
    plt.figure(figsize=(10, 6))
    ax = plt.gca()  # Get current axis
    for idx, k in enumerate(scores[0].fids_per_class.keys()):
        # [existing plot code]

        # Enable and customize the spines for the frame
        for spine in ax.spines.values():
            spine.set_visible(True)
            spine.set_color("black")
            spine.set_linewidth(1)

        for metric in scores:
            plt.plot(
                metric.epoch,
                metric.fids_per_class[k],
                color=COLORS[idx % len(COLORS)],
                marker=MARKERS[idx % len(MARKERS)],
                linestyle=LINESTYLES[idx % len(LINESTYLES)],
                label=f"{metric.label} - {k}",
                markersize=4,
            )

    plt.title("FID per Class over Epochs", fontsize=14)
    plt.xlabel("Epoch", fontsize=12)
    plt.ylabel("FID", fontsize=12)
    plt.xticks(fontsize=10)
    plt.yticks(fontsize=10)
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    legend = plt.legend(
        frameon=True,
        facecolor="white",
        edgecolor="black",
        shadow=True,
        framealpha=1.0,
        loc="upper right",  # Adjust as needed for best fit
        fontsize="small",  # Adjust font size to make the legend readable
    )
    legend.set_zorder(3)
    plt.grid(True, linestyle="--", color="gray", linewidth=0.5, alpha=0.7)
    plt.tight_layout()

    # Save the plot with a general name
    plt.savefig(
        f"{save_path}_all_classes.png", format="png", dpi=300
    )  # Save as high-quality PNG
    plt.show()
    plt.close()


def plot_metric_new(
    scores: List[PlotData], title, y_label, save_path, metric_type: Metric
):
    plt.figure(figsize=(10, 6))
    ax = plt.gca()

    for spine in ax.spines.values():
        spine.set_visible(True)
        spine.set_color("black")
        spine.set_linewidth(1)

    for i, score in enumerate(scores):
        if metric_type == Metric.FID_PER_CLASS:
            for class_key, class_data in score.fids_per_class.items():
                plt.plot(
                    score.epoch,
                    class_data,
                    color=COLORS[i % len(COLORS)],
                    marker=MARKERS[i % len(MARKERS)],
                    linestyle=LINESTYLES[i % len(LINESTYLES)],
                    label=f"{score.label} - {class_key}",
                    markersize=4,
                )
        else:
            metric_data = {
                "fid": score.fid,
                "inception_mean": score.inception_mean,
                "inception_std": score.inception_std,
                # Add other metrics here if needed
            }.get(metric_type.value, [])

            plt.plot(
                score.epoch,
                metric_data,
                color=COLORS[i % len(COLORS)],
                marker=MARKERS[i % len(MARKERS)],
                linestyle=LINESTYLES[i % len(LINESTYLES)],
                label=score.label,
                markersize=4,
            )

    plt.title(title, fontsize=14)
    plt.xlabel("Epoch", fontsize=12)
    plt.ylabel(y_label, fontsize=12)
    plt.xticks(fontsize=10)
    plt.yticks(fontsize=10)
    plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
    legend = plt.legend(
        frameon=True,
        facecolor="white",
        edgecolor="black",
        shadow=True,
        framealpha=1.0,
    )
    legend.set_zorder(3)
    plt.grid(True, linestyle="--", color="gray", linewidth=0.5, alpha=0.7)
    plt.tight_layout()
    plt.savefig(save_path, format="png", dpi=300)  # Save as high-quality PNG
    plt.show()
    plt.close()


def plot_all(to_be_plotted: List[Res], save_dir, start: int = 1, end: int = 200):
    plot_data_collection = []
    for res in to_be_plotted:
        eval_path = res.root / "evals"
        if not os.path.exists(eval_path):
            logging.error("Path does not exist: %s", eval_path)
            continue

        epochs, fids, inception_means, inception_stds, fids_per_class = read_data(
            eval_path, start=start, end=end
        )

        plot_data_collection.append(
            PlotData(
                epoch=epochs,
                fid=fids,
                inception_mean=inception_means,
                inception_std=inception_stds,
                fids_per_class=fids_per_class,
                label=res.name,
            )
        )

    # # Plot and Save FID
    # plot_metric_new(
    #     scores=plot_data_collection,
    #     title="FID over Epochs",
    #     y_label="FID",
    #     save_path=f"{save_dir}/fid_plot.png",
    #     metric_type=Metric.FID,
    # )
    #
    # # Plot and Save Inception Score Mean
    # plot_metric_new(
    #     scores=plot_data_collection,
    #     title="Inception Score Mean over Epochs",
    #     y_label="Inception Score Mean",
    #     save_path=f"{save_dir}/inception_score_mean_plot.png",
    #     metric_type=Metric.INCEPTION_MEAN,
    # )

    # # Plot and Save Inception Score Mean
    # plot_metric_new(
    #     scores=plot_data_collection,
    #     title="Inception Score STD over Epochs",
    #     y_label="Inception Score STD",
    #     save_path=f"{save_dir}/inception_score_std_plot.png",
    #     metric_type=Metric.INCEPTION_STD,
    # )
    #
    # # Plot and Save FID per Class
    # plot_fid_per_class(
    #     scores=plot_data_collection,
    #     save_path=f"{save_dir}/fid",
    # )
    # Plot and Save FID per Class
    plot_fid_per_class_singular(
        scores=plot_data_collection,
        save_path=f"{save_dir}/fid",
    )


def plot_base_vs_singles():
    for model in [
        "aug",
        # "b1",
        # "lrd",
        # "lrd_1",
        # "ls9",
        # "lscan",
        # "lsgan",
        # "mb",
        # "noise300",
        # "spectral",
        # "transposed",
        # "lrd_lrg",
        # "lrd_lscn_sctrl",
        # "transposed_lrd_lscn_spctrl",
    ]:
        save_path = f"/Users/arvandkaveh/Projects/kit/pnn/plots/base_vs_{model}"
        os.makedirs(save_path, exist_ok=True)
        # tags
        # b: base, main: main task, x: extras, 128: gen18, 512: gen512, t: transposed
        filtered_results = filter_by_tags(
            results=AVAILABLE_RESULTS,
            include_tags={f"base_{model}"},
            exclude_tags={"128"},
            # include_single_tag={f"base_{model}"},
            add_base=False,
        )
        plot_all(
            to_be_plotted=filtered_results,
            save_dir=save_path,
            start=start,
            end=end,
        )


def plot_transpose_vs():
    for model in [
        "aug",
        "betas",
        "lrd",
        "lrd_lrg",
        "lrd_lrg_lscn_spctrl_ls9_b1",
        "lrd_lscn_spctrl",
        "ls9",
    ]:
        save_path = (
            f"/Users/arvandkaveh/Projects/kit/pnn/plots/base_transposed_vs_{model}"
        )
        os.makedirs(save_path, exist_ok=True)
        # tags
        # b: base, main: main task, x: extras, 128: gen18, 512: gen512, t: transposed
        plot_all(
            to_be_plotted=filter_by_tags(
                results=AVAILABLE_RESULTS,
                include_tags={"base_transposed"},
                include_single_tag={f"base_transposed_{model}"},
                add_base=False,
            ),
            save_dir=save_path,
            start=start,
            end=end,
        )


def plot_transpose_mb_vs():
    for model in [
        "b1",
        "b1_lr",
        "b1_lr_ls9",
        "b1_lr_ls9_spct",
        "b1_lr_ls9_spct_ls",
    ]:
        save_path = (
            f"/Users/arvandkaveh/Projects/kit/pnn/plots/base_transposed_mb_vs_{model}"
        )
        os.makedirs(save_path, exist_ok=True)
        # tags
        # b: base, main: main task, x: extras, 128: gen18, 512: gen512, t: transposed
        plot_all(
            to_be_plotted=filter_by_tags(
                results=AVAILABLE_RESULTS,
                include_tags={"base"},
                include_single_tag={f"base_{model}"},
                add_base=False,
            ),
            save_dir=save_path,
            start=start,
            end=end,
        )


if __name__ == "__main__":
    # Example usage
    start = 1
    end = 200
    plot_base_vs_singles()
    # plot_transpose_vs()
    # plot_transpose_mb_vs()
