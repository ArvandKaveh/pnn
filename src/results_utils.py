import heapq
import json
from collections import namedtuple
from dataclasses import dataclass
from pathlib import Path
from typing import Tuple, Dict, List

import pandas as pd

from logger import get_logger
from models.generator import (
    Generator,
    Generator128D02L4,
    Generator512D02L5,
    Generator128D02L4Transposed,
    Generator512D02L5Transposed,
)

logger = get_logger("ResultsUtils")

Res = namedtuple("Res", ["root", "name", "tags"])


@dataclass
class Res:
    root: Path
    name: str
    tags: set[str]
    generator: Generator = Generator128D02L4
    noise_dim: int = 100
    use_leaky_relu: bool = False


BASE_DIR = str(Path.cwd().parent) + "/res"
path_512_fmt = BASE_DIR + "/{}/Generator512D02L5"
path_512t_fmt = BASE_DIR + "/{}/Generator512D02L5Transpose"
path_128_fmt = BASE_DIR + "/{}/Generator128D02L4"
path_128t_fmt = BASE_DIR + "/{}/Generator128D02L4Transpose"

AVAILABLE_RESULTS: list[Res] = [
    Res(
        root=Path(path_128_fmt.format("base")),
        name="Generator128L4:Base",
        tags={"b", "main", "128", "base", "base_128"},
    ),
    Res(
        root=Path(path_512_fmt.format("base")),
        name="Generator512L5:Base",
        tags={"b", "main", "512", "base", "base_512"},
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_512_fmt.format("base_aug")),
        name="Generator512L5:Base+Augmented",
        tags={"b", "x", "512", "single", "base_aug"},
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_128_fmt.format("base_b1")),
        name="Generator128L4:Base+Adam(beta1=0.5)",
        tags={"b", "x", "128", "single", "base_b1"},
    ),
    Res(
        root=Path(path_512_fmt.format("base_b1")),
        name="Generator512L5:Base+Adam(beta1=0.5)",
        tags={"b", "x", "512", "single", "base_b1"},
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_128_fmt.format("base_lrd")),
        name="Generator128L4:Base+LReluD(0.2)",
        tags={"b", "main", "128", "single", "base_lrd"},
    ),
    Res(
        root=Path(path_512_fmt.format("base_lrd")),
        name="Generator512L5:Base+LReluD(0.2)",
        tags={"b", "main", "512", "single", "base_lrd"},
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_128_fmt.format("base_lrd_1")),
        name="Generator128L4:Base+LReluD(0.1)",
        tags={"b", "main", "128", "single", "base_lrd_1"},
    ),
    Res(
        root=Path(path_512_fmt.format("base_lrd_1")),
        name="Generator512L5:Base+LReluD(0.1)",
        tags={"b", "main", "512", "single", "base_lrd_1"},
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_128_fmt.format("base_lrd_lrg")),
        name="Generator128L4:Base+LReluD+LReluG",
        tags={"b", "x", "128", "base_lrd_lrg"},
        use_leaky_relu=True,
    ),
    Res(
        root=Path(path_512_fmt.format("base_lrd_lrg")),
        name="Generator512L5:Base+LReluD+LReluG",
        tags={"b", "x", "512", "base_lrd_lrg"},
        use_leaky_relu=True,
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_128_fmt.format("base_lrd_lscn_spctrl")),
        name="Generator128L4:Base+LReluD+LeastSquare+SpectralNorm",
        tags={"b", "x", "128", "base_lrd_lscn_spctrl"},
    ),
    Res(
        root=Path(path_512_fmt.format("base_lrd_lscn_spctrl")),
        name="Generator512L5:Base+LReluD+LeastSquare+SpectralNorm",
        tags={"b", "x", "512", "base_lrd_lscn_spctrl"},
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_128_fmt.format("base_ls9")),
        name="Generator128L4:Base+LabelSmoothing(0.9)",
        tags={"b", "x", "128", "single", "base_ls9"},
    ),
    Res(
        root=Path(path_512_fmt.format("base_ls9")),
        name="Generator512L5:Base+LabelSmoothing(0.9)",
        tags={"b", "x", "512", "single", "base_ls9"},
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_128_fmt.format("base_ls9_2")),
        name="Generator128L4:Base+LabelSmoothing2(0.9)",
        tags={"b", "x", "128", "single", "base_ls9"},
    ),
    Res(
        root=Path(path_512_fmt.format("base_ls9_2")),
        name="Generator512L5:Base+LabelSmoothing2(0.9)",
        tags={"b", "x", "512", "single", "base_ls9"},
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_128_fmt.format("base_lsgan")),
        name="Generator128L4:Base+LeastSquareGAN",
        tags={"b", "main", "128", "single", "base_lsgan"},
        generator=Generator128D02L4,
    ),
    Res(
        root=Path(path_512_fmt.format("base_lsgan")),
        name="Generator512L5:Base+LeastSquareGAN",
        tags={"b", "main", "512", "single", "base_lsgan"},
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_128_fmt.format("base_mb")),
        name="Generator128L4:Base+MinibatchDiscrimination",
        tags={"b", "x", "128", "mb", "single", "base_mb"},
    ),
    Res(
        root=Path(path_512_fmt.format("base_mb")),
        name="Generator512L5:Base+MinibatchDiscrimination",
        tags={"b", "x", "512", "mb", "single", "base_mb"},
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_128_fmt.format("base_mb_b1")),
        name="Generator128L4:Base+MinibatchDiscrimination+Adam(beta1=0.5)",
        tags={"b", "x", "128", "mb", "base_mb_b1"},
    ),
    Res(
        root=Path(path_512_fmt.format("base_mb_b1")),
        name="Generator512L5:Base+MinibatchDiscrimination+Adam(beta1=0.5)",
        tags={"b", "x", "512", "mb", "base_mb_b1"},
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_512_fmt.format("base_noise300")),
        name="Generator512L5:Base+NoiseDimension300",
        tags={"b", "x", "512", "single", "base_noise300"},
        noise_dim=300,
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_128_fmt.format("base_spectral")),
        name="Generator128L4:Base+SpectralNorm",
        tags={"b", "main", "128", "single", "base_spectral"},
    ),
    Res(
        root=Path(path_512_fmt.format("base_spectral")),
        name="Generator512L5:Base+SpectralNorm",
        tags={"b", "main", "512", "single", "base_spectral"},
        generator=Generator512D02L5,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed")),
        name="Generator128L4:Base+Transposed",
        tags={"main", "128", "b", "t", "single", "base_transposed"},
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed")),
        name="Generator512L5:Base+Transposed",
        tags={"main", "512", "b", "t", "single", "transposed", "base_transposed"},
        generator=Generator512D02L5Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_aug")),
        name="Generator512L5:Base+Transpose+Augmented",
        tags={"x", "512", "t", "base_transposed_aug"},
        generator=Generator512D02L5Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_lrd")),
        name="Generator512L5:Base+Transpose+LReluD",
        tags={"x", "512", "t", "single", "base_transposed_lrd"},
        generator=Generator512D02L5Transposed,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_lrd_lrg")),
        name="Generator128L4:Base+Transpose+LReluD+LReluG",
        tags={"x", "128", "t", "base_transposed_lrd_lrg"},
        use_leaky_relu=True,
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_lrd_lrg")),
        name="Generator512L5:Base+Transpose+LReluD+LReluG",
        tags={"x", "512", "t", "base_transposed_lrd_lrg"},
        use_leaky_relu=True,
        generator=Generator512D02L5Transposed,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_lrd_lrg_lscn_spctrl_ls9_b1")),
        name=("Generator128L4:Base+Transpose+LRD+LRG+LS+SN+LS(0.9)+Adam(beta1=0.5)"),
        tags={"x", "128", "t", "base_transposed_lrd_lrg_lscn_spctrl_ls9_b1"},
        use_leaky_relu=True,
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_lrd_lrg_lscn_spctrl_ls9_b1")),
        name=("Generator512L5:Base+Transpose+LRD+LRG+LS+SN+LS(0.9)+Adam(beta1=0.5)"),
        tags={"x", "512", "t", "base_transposed_lrd_lrg_lscn_spctrl_ls9_b1"},
        use_leaky_relu=True,
        generator=Generator512D02L5Transposed,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_lrd_lscn_spctrl")),
        name="Generator128L4:Base+Transpose+LReluD+LeastSquare+SpectralNorm",
        tags={"x", "128", "t", "base_transposed_lrd_lscn_spctrl"},
        use_leaky_relu=True,
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_lrd_lscn_spctrl")),
        name="Generator512L5:Base+Transpose+LReluD+LeastSquare+SpectralNorm",
        tags={"x", "512", "t", "base_transposed_lrd_lscn_spctrl"},
        use_leaky_relu=True,
        generator=Generator512D02L5Transposed,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_ls9")),
        name="Generator128L4:Base+Transpose+LabelSmoothing(0.9)",
        tags={"x", "128", "t", "base_transposed_ls9"},
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_ls9")),
        name="Generator512L5:Base+Transpose+LabelSmoothing(0.9)",
        tags={"x", "512", "t", "base_transposed_ls9"},
        generator=Generator512D02L5Transposed,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_ls9_2")),
        name="Generator128L4:Base+Transpose+LabelSmoothing2(0.9)",
        tags={"x", "128", "t", "base_transposed_ls9_2"},
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_ls9_2")),
        name="Generator512L5:Base+Transpose+LabelSmoothing2(0.9)",
        tags={"x", "512", "t", "base_transposed_ls9_2"},
        generator=Generator512D02L5Transposed,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_mb")),
        name="Generator128L4:Base+Transpose+MinibatchDiscrimination",
        tags={"x", "128", "t", "mb", "base_transposed_mb"},
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_mb")),
        name="Generator512L5:Base+Transpose+MinibatchDiscrimination",
        tags={"x", "512", "t", "mb", "base_transposed_mb"},
        generator=Generator512D02L5Transposed,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_mb_b1")),
        name="Generator128L4:Base+Transpose+MD+A(b1=0.5)",
        tags={"x", "128", "t", "mb", "base_transposed_mb_b1"},
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_mb_b1")),
        name="Generator512L5:Base+Transpose+MD+A(b1=0.5)",
        tags={"x", "512", "t", "mb", "base_transposed_mb_b1"},
        generator=Generator512D02L5Transposed,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_mb_b1_lr")),
        name="Generator128L4:Base+Transpose+MD+A(b1=0.5)+LR",
        tags={"x", "128", "t", "mb", "base_transposed_mb_b1_lr"},
        use_leaky_relu=True,
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_mb_b1_lr")),
        name="Generator512L5:Base+Transpose+MD+A(b1=0.5)+LR",
        tags={"x", "512", "t", "mb", "base_transposed_mb_b1_lr"},
        generator=Generator512D02L5Transposed,
        use_leaky_relu=True,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_mb_b1_lr_ls9")),
        name="Generator128L4:Base+Transpose+MD+A(b1=0.5)+LR+LS9",
        tags={"x", "128", "t", "mb", "base_transposed_mb_b1_lr_ls9"},
        use_leaky_relu=True,
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_mb_b1_lr_ls9")),
        name="Generator512L5:Base+Transpose+MD+A(b1=0.5)+LR+LS9",
        tags={"x", "512", "t", "mb", "base_transposed_mb_b1_lr_ls9"},
        generator=Generator512D02L5Transposed,
        use_leaky_relu=True,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_mb_b1_lr_ls9_spct")),
        name="Generator128L4:Base+Transpose+MD+A(b1=0.5)+LR+LS9+SN",
        tags={"x", "128", "t", "mb", "base_transposed_mb_b1_lr_ls9_spct"},
        use_leaky_relu=True,
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_mb_b1_lr_ls9_spct")),
        name="Generator512L5:Base+Transpose+MD+A(b1=0.5)+LR+LS9+SN",
        tags={"x", "512", "t", "mb", "base_transposed_mb_b1_lr_ls9_spct"},
        generator=Generator512D02L5Transposed,
        use_leaky_relu=True,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_mb_b1_lr_ls9_spct_ls")),
        name="Generator128L4:Base+Transpose+MD+A(b1=0.5)+LR+LS9+SN+LSL",
        tags={"x", "128", "t", "mb", "base_transposed_mb_b1_lr_ls9_spct_ls"},
        use_leaky_relu=True,
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_mb_b1_lr_ls9_spct_ls")),
        name="Generator512L5:Base+Transpose+MD+A(b1=0.5)+LR+LS9+SN+LSL",
        tags={"x", "512", "t", "mb", "base_transposed_mb_b1_lr_ls9_spct_ls"},
        generator=Generator512D02L5Transposed,
        use_leaky_relu=True,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_mb_b1_lrd_ls9")),
        name="Generator128L4:Base+Transpose+MD+A(b1=0.5)+LRD+LS9",
        tags={"x", "128", "t", "mb", "base_transposed_mb_b1_lrd_ls9"},
        use_leaky_relu=True,
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_mb_b1_lrd_ls9")),
        name="Generator512L5:Base+Transpose+MD+A(b1=0.5)+LRD+LS9",
        tags={"x", "512", "t", "mb", "base_transposed_mb_b1_lrd_ls9"},
        generator=Generator512D02L5Transposed,
        use_leaky_relu=True,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_mb_b1_lrd_ls9_spct")),
        name="Generator128L4:Base+Transpose+MD+A(b1=0.5)+LRD+LS9+SN",
        tags={"x", "128", "t", "mb", "base_transposed_mb_b1_lrd_ls9_spct"},
        use_leaky_relu=True,
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_mb_b1_lrd_ls9_spct")),
        name="Generator512L5:Base+Transpose+MD+A(b1=0.5)+LRD+LS9+SN",
        tags={"x", "512", "t", "mb", "base_transposed_mb_b1_lrd_ls9_spct"},
        generator=Generator512D02L5Transposed,
        use_leaky_relu=True,
    ),
    Res(
        root=Path(path_128t_fmt.format("base_transposed_mb_b1_lrd_ls9_spct_ls")),
        name="Generator128L4:Base+Transpose+MD+A(b1=0.5)+LRD+LS9+SN+LS",
        tags={"x", "128", "t", "mb", "base_transposed_mb_b1_lrd_ls9_spct_ls"},
        use_leaky_relu=True,
        generator=Generator128D02L4Transposed,
    ),
    Res(
        root=Path(path_512t_fmt.format("base_transposed_mb_b1_lrd_ls9_spct_ls")),
        name="Generator512L5:Base+Transpose+MD+A(b1=0.5)+LRD+LS9+SN+LS",
        tags={"x", "512", "t", "mb", "base_transposed_mb_b1_lrd_ls9_spct_ls"},
        generator=Generator512D02L5Transposed,
        use_leaky_relu=True,
    ),
    Res(
        root=Path(path_512t_fmt.format("test_run")),
        name="TEST RUN",
        tags={"test_run"},
        generator=Generator512D02L5Transposed,
        use_leaky_relu=True,
    ),
]


def get_checkpoint(root: Path, number: int):
    path = root / get_checkpoint_name(number)
    if not path.is_file():
        logger.error("File does not exist: %s" % path)
    return path


def get_checkpoint_name(number) -> Path:
    checkpoint_file_name = f"generator_{number:03}"
    return Path(checkpoint_file_name)


def filter_by_tags(
    results: list[Res],
    include_tags: set[str],
    exclude_tags: set[str] = None,
    include_single_tag: [list[str]] = list(),
    add_base: bool = False,
) -> list[Res]:
    result = []
    if add_base:
        result.append(AVAILABLE_RESULTS[0])
        # result.append(AVAILABLE_RESULTS[1])

    # # Deal with single tag
    # for res in results:
    #     if any(tag for tag in include_single_tag if tag in res.tags):
    #         result.append(res)

    for res in results:
        if include_tags.issubset(res.tags) and (
            exclude_tags is None or not exclude_tags.intersection(res.tags)
        ):
            result.append(res)

    # Deal with single tag
    for res in results:
        if any(tag for tag in include_single_tag if tag in res.tags) and (
            exclude_tags is None or not exclude_tags.intersection(res.tags)
        ):
            result.append(res)
    return result


def top_n_epochs(
    epochs: List[int], values: List[float], n: int, reverse: bool = False
) -> List[Tuple[int, float]]:
    """Returns top N epochs and their values."""
    if len(epochs) != len(values):
        raise ValueError("Epochs and values lists must have the same length.")

    combined = list(zip(epochs, values))
    if reverse:
        top_n = heapq.nsmallest(n, combined, key=lambda x: x[1])
    else:
        top_n = heapq.nlargest(n, combined, key=lambda x: x[1])

    return top_n


def get_top_n_per_kpi(data: Tuple, n: int) -> Dict[str, List[Tuple[int, float]]]:
    epochs, fids, inception_means, inception_stds, fids_per_class = data

    top_n_results = {
        "fid": top_n_epochs(epochs, fids, n, reverse=True),
        "inception_score_mean": top_n_epochs(epochs, inception_means, n),
        "inception_score_std": top_n_epochs(epochs, inception_stds, n),
    }

    # Add FIDs per class
    for k, v in fids_per_class.items():
        top_n_results[f"fid_per_class_{k}"] = top_n_epochs(epochs, v, n, reverse=True)

    return top_n_results


def find_best_fids(res_list: List[Res], n: int) -> Dict[str, Tuple[int, float, str]]:
    best_overall_fid = (
        "Overall",
        (float("inf"), -1, ""),
    )  # (class, (fid, epoch, model))
    best_fids_per_class = {}  # class: (fid, epoch, model)

    for res in res_list:
        top_n_kpi = get_top_n_per_kpi(read_data(res.root / "evals"), n)
        model_name = res.name  # Use the name from the Res namedtuple

        # Check overall best FID
        overall_fid = min(top_n_kpi["fid"], key=lambda x: x[1])
        if overall_fid[1] < best_overall_fid[1][0]:
            best_overall_fid = ("Overall", (overall_fid[1], overall_fid[0], model_name))

        # Check best FIDs per class
        for k in top_n_kpi.keys():
            if k.startswith("fid_per_class_"):
                class_name = k[len("fid_per_class_") :]

                best_fid_for_class = min(top_n_kpi[k], key=lambda x: x[1])
                if (
                    class_name not in best_fids_per_class
                    or best_fid_for_class[1] < best_fids_per_class[class_name][0]
                ):
                    best_fids_per_class[class_name] = (
                        best_fid_for_class[1],
                        best_fid_for_class[0],
                        model_name,
                    )

    return {"best_overall_fid": best_overall_fid, **best_fids_per_class}


def list_files(directory: Path) -> list:
    return sorted([file.name for file in directory.iterdir() if file.is_file()])


def read_data(directory: Path, start: int = 1, end: int = 200):
    fids = []
    inception_means = []
    inception_stds = []
    fids_per_class = {
        k: []
        for k in [
            "airplane",
            "automobile",
            "bird",
            "cat",
            "deer",
            "dog",
            "frog",
            "horse",
            "ship",
            "truck",
        ]
    }
    # [0, 1, 2 ..., 200]
    # [
    # {"airlpant": [
    epochs = []

    for i in range(start, end):
        filename = f"epoch_history_{i:03}.json"
        filepath = directory / filename
        if filepath.is_file():
            with open(filepath, "r") as file:
                data = json.load(file)
                fids.append(data["fid"])
                inception_means.append(data["inception_score_mean"])
                inception_stds.append(data["inception_score_std"])
                for k in fids_per_class.keys():
                    fids_per_class[k].append(data["fid_per_class"][k])
                epochs.append(data["epoch"])

    return epochs, fids, inception_means, inception_stds, fids_per_class


def read_pd(directory: Path, start: int = 1, end: int = 200) -> pd.DataFrame:
    fids = []
    inception_means = []
    inception_stds = []
    fids_per_class = {
        k: []
        for k in [
            "airplane",
            "automobile",
            "bird",
            "cat",
            "deer",
            "dog",
            "frog",
            "horse",
            "ship",
            "truck",
        ]
    }
    # [0, 1, 2 ..., 200]
    # [
    # {"airlpant": [
    epochs = []

    for i in range(start, end):
        filename = f"epoch_history_{i:03}.json"
        filepath = directory / filename
        if filepath.is_file():
            with open(filepath, "r") as file:
                data = json.load(file)
                fids.append(data["fid"])
                inception_means.append(data["inception_score_mean"])
                inception_stds.append(data["inception_score_std"])
                for k in fids_per_class.keys():
                    fids_per_class[k].append(data["fid_per_class"][k])
                epochs.append(data["epoch"])

    df = pd.DataFrame(
        {
            "epoch": epochs,
            "fid": fids,
            "inception_mean": inception_means,
            "inception_std": inception_stds,
        }
    )
    df["fid"] = df["fid"].astype("int")
    df["inception_mean"] = df["inception_mean"].map(lambda x: f"{x:.2f}")
    df["inception_std"] = df["inception_std"].map(lambda x: f"{x:.3f}")

    for k, v in fids_per_class.items():
        df[k] = v
        df[k] = df[k].astype("int")

    return df


def filter_epochs(res: Res, epochs: list[int]):
    df = read_pd(res.root / "evals")
    return df[df["epoch"].isin(epochs)]


def list_filtered_epochs(results: list[Res], epochs: list[int]):
    df = pd.DataFrame()
    for res in results:
        best_epochs_res = filter_epochs(res, epochs)
        best_epochs_res["res"] = res.name
        df = pd.concat([df, best_epochs_res])
    return df


def filter_best_epoch(res: Res):
    df = read_pd(res.root / "evals")
    return df[df["fid"] == df["fid"].min()]


def list_best_epochs(results: list[Res]):
    df = pd.DataFrame()
    for res in results:
        best_epochs_res = filter_best_epoch(res)
        best_epochs_res["res"] = res.name
        df = pd.concat([df, best_epochs_res])
    return df


if __name__ == "__main__":
    # test_dir = AVAILABLE_RESULTS[0].root / "evals"
    first_res = AVAILABLE_RESULTS[0]
    # print(read_pd(test_dir).head())
    # print(filter_epochs(first_res, [10, 20, 30, 40, 50]).head())
    # print(filter_best_epoch(first_res))
    print(len(AVAILABLE_RESULTS))
    # Get the best
    # best_epoch_table = list_best_epochs(AVAILABLE_RESULTS)
    # root = Path("/Users/arvandkaveh/Projects/kit/pnn") / "best_models_fid_overall.csv"
    # best_epoch_table.to_csv(root)

    # # Get filtered
    # df = list_filtered_epochs(AVAILABLE_RESULTS, [25, 50, 75, 100, 125, 150, 175, 200])
    # root = Path("/Users/arvandkaveh/Projects/kit/pnn") / "selected_epochs.csv"
    # df.to_csv(root)
    print(Path.cwd())
    print(Path("../res"))
