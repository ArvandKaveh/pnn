from pathlib import Path

import torch

from src.config import Config
from src.data.cifar_10_dataset_provider import Cifar10DatasetProvider
from src.evaluation.fid_calculator import FidCalculator
from src.logger import get_logger
from src.models.discriminator import DiscriminatorBasic
from src.models.generator import get_generator
from src.parser import get_cli_args
from src.trainer import Trainer

logger = get_logger("TrainingRunnerBase")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def main(args):
    home_dir = Path(args.home_dir)
    logger.info("Starting the training process")
    logger.info("With the following arguments: %s", args)
    dataset_provider = Cifar10DatasetProvider(batch_size=args.batch_size)
    fid_calculator = FidCalculator(
        data_provider=dataset_provider, num_images_per_class=100, device=device
    )
    for generator_class in [
        get_generator(short_name=short_name) for short_name in args.generators
    ]:
        generator = generator_class(
            use_leaky_relu=args.use_leaky_relu_generator,
            noise_dim=args.noise_dim,
            use_batch_normalisation_generator=args.use_batch_normalisation_generator,
        ).to(device)
        discriminator = DiscriminatorBasic(
            use_leaky_relu=args.use_leaky_relu_discriminator,
            use_spectral_norm=args.use_spectral_norm,
            use_minibatch_discriminator=args.use_minibatch_discriminator,
            mb_discrim_out_features=100 if args.use_minibatch_discriminator else None,
        ).to(device)

        logger.info("Initializing training for generator: %s", generator.NAME)
        trainer = Trainer(
            generator=generator,
            discriminator=discriminator,
            data_provider=dataset_provider,
            config=Config(
                name=generator.NAME,
                base_path=home_dir,
                batch_size=args.batch_size,
                noise_dim=args.noise_dim,
                num_epochs=args.num_epochs,
                avg_window_size=args.window_size,
                use_augmentation=args.use_augmentation,
                label_smoothing_factor=args.label_smoothing_factor,
                log_tensorboard=args.log_tensorboard,
            ),
            criterion=args.criterion.loss_function(),
            optimizer_generator=torch.optim.Adam(
                generator.parameters(),
                lr=args.learning_rate,
                betas=(args.beta1, args.beta2),
            ),
            optimizer_discriminator=torch.optim.Adam(
                discriminator.parameters(),
                lr=args.learning_rate,
                betas=(args.beta1, args.beta2),
            ),
            fid_calculator=fid_calculator,
        )
        try:
            trainer.train(args.is_running_eval)
        except Exception as e:
            logger.error("Error while training generator: %s", generator.NAME)
            logger.error("Error: %s", e)
            logger.error("Continuing to train the next generator")
            trainer.save()
            continue
        finally:
            logger.info("Finished training for generator: %s", generator.NAME)


if __name__ == "__main__":
    main(get_cli_args())
