import json
from datetime import datetime

import torch
import torchvision
from torch.utils.tensorboard import SummaryWriter

from src import utils
from src.config import Config, TrainingHistory, EpochHistory
from src.data.cifar_10_dataset_provider import Cifar10DatasetProvider
from src.evaluation.fid_calculator import FidCalculator
from src.evaluation.inception_score import (
    inception_score,
    transform_for_inception_score,
)
from src.logger import get_logger
from src.models.discriminator import Discriminator
from src.models.generator import Generator

logger = get_logger("Trainer")


class Trainer:
    def __init__(
        self,
        generator: Generator,
        discriminator: Discriminator,
        data_provider: Cifar10DatasetProvider,
        config: Config,
        criterion,
        optimizer_generator,
        optimizer_discriminator,
        fid_calculator,
    ):
        self.generator = generator
        self.discriminator = discriminator
        self.data_provider = data_provider
        self.config = config
        self._termination_counter = 0
        self.real_images = None
        self.real_labels_split = None
        self.fid_calculator: FidCalculator = fid_calculator

        # The loss function
        self.criterion = criterion

        # For backpropagation optimizers
        self.optimizer_generator = optimizer_generator
        self.optimizer_discriminator = optimizer_discriminator

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        if self.config.log_tensorboard:
            self.writer_fake = SummaryWriter(
                str(self.config.log_path / "runs/CIFAR10_fake")
            )
            self.writer_real = SummaryWriter(
                str(self.config.log_path / "runs/CIFAR10_real")
            )
            self.fixed_noise = torch.randn(
                self.config.batch_size, self.config.noise_dim, 1, 1
            ).to(self.device)
            self.fixed_labels = torch.randint(
                0, self.config.num_classes, (self.config.batch_size,)
            ).to(self.device)

        self.training_history: TrainingHistory = TrainingHistory(
            top_models_size=self.config.keep_best_models
        )
        self._log_trainer_info()

    def train(self, with_evaluation: bool = False):
        logger.debug(
            f"Starting the training process for generator: {self.generator.NAME}"
        )
        step = 0

        for epoch in utils.iterate_over(range(1, self.config.num_epochs + 1)):
            logger.debug(f"Starting epoch {epoch}")
            epoch_start_time = datetime.now()
            for batch_idx, (real_image, real_label) in enumerate(
                self.data_provider.get_loader(
                    is_train=True, is_augment=self.config.use_augmentation
                )
            ):
                # Initialize input tensors
                real_image = real_image.to(self.device)
                real_label = real_label.to(self.device)
                noise = torch.randn(
                    real_image.size(0), self.config.noise_dim, device=self.device
                )
                fake_labels = torch.randint(
                    0, self.config.num_classes, (real_image.size(0),)
                ).to(self.device)
                ones = (
                    torch.ones(real_image.size(0), 1, device=self.device)
                    * self.config.label_smoothing_factor
                ).squeeze(1)

                # Training Discriminator and Generator
                self._train_discriminator(
                    fake_labels, noise, ones, real_image, real_label
                )
                self._train_generator(fake_labels, noise, ones)

                # Logging and visualization with TensorBoard
                if batch_idx == 0 and self.config.log_tensorboard:
                    step = self._log_tensorboard(real_image, step)

            # Print some statistics
            logger.debug(
                f"Epoch [{epoch}/{self.config.num_epochs}]. "
                f"Time taken for epoch {(datetime.now() - epoch_start_time).seconds} seconds"
            )

            # Evaluate the model sofar
            epoch_history = EpochHistory(epoch=epoch)
            if with_evaluation and (
                (epoch >= self.config.start_evaluating_from_epoch) or (epoch % 10 == 0)
            ):
                epoch_history = self.evaluate(epoch, epoch_history)
            self.training_history.add(epoch_history)

            if epoch_history.should_save_model:
                epoch_history.model_parameters = self.generator.state_dict()

            logger.info(epoch_history)
        self.save()

    def _log_tensorboard(self, real_image, step):
        with torch.no_grad():
            fake_images = self.generator(self.fixed_noise, self.fixed_labels)
            img_grid_fake = torchvision.utils.make_grid(fake_images, normalize=True)
            img_grid_real = torchvision.utils.make_grid(real_image, normalize=True)

            self.writer_fake.add_image(
                "CIFAR10 Fake Images", img_grid_fake, global_step=step
            )
            self.writer_real.add_image(
                "CIFAR10 Real Images", img_grid_real, global_step=step
            )

        return step + 1

    def _train_generator(self, fake_labels, noise, ones):
        self._disable_models_gradients(self.discriminator)
        disc_output_on_gen_img = self.discriminator(
            self.generator(noise, fake_labels), fake_labels
        ).view(-1)
        loss_generator = self.criterion(disc_output_on_gen_img, ones)
        self.optimizer_generator.zero_grad()
        loss_generator.backward()
        self.optimizer_generator.step()
        self._enable_models_gradients(self.discriminator)

    def _train_discriminator(self, fake_labels, noise, ones, real_image, real_label):
        self._disable_models_gradients(self.generator)
        fake_image = self.generator(noise, fake_labels)
        # Compute discriminator loss on real_image and fake_image images
        disc_real = self.discriminator(real_image, real_label).view(-1)
        loss_disc_real = self.criterion(disc_real, ones)
        self.discriminator.zero_grad()
        disc_fake = self.discriminator(fake_image.detach(), fake_labels).view(-1)
        zeros = (
            torch.zeros(real_image.size(0), 1, device=self.device)
            + (1 - self.config.label_smoothing_factor)
        ).squeeze(1)
        loss_disc_fake = self.criterion(disc_fake, zeros)
        loss_disc = loss_disc_real + loss_disc_fake
        loss_disc.backward()
        self.optimizer_discriminator.step()
        self._enable_models_gradients(self.generator)

    @staticmethod
    def _disable_models_gradients(model):
        for param in model.parameters():
            param.requires_grad = False

    @staticmethod
    def _enable_models_gradients(model):
        for param in model.parameters():
            param.requires_grad = True

    def evaluate(self, epoch_num, epoch_history):
        logger.debug(f"Evaluating epoch {epoch_num}")
        self.generator.eval()
        generated_images = transform_for_inception_score(
            self.generate_images(self.config.evaluation_num_images_per_class),
            self.device,
        )

        logger.debug(f"generated_images.shape: {generated_images.shape}")
        logger.debug(f"generate images type: {type(generated_images)}")
        (
            epoch_history.inception_score_mean,
            epoch_history.inception_score_std,
        ) = self._evaluate_inception_score(generated_images)
        self.fid_calculator.set_generated_images(
            generated_images=generated_images,
            num_classes=self.data_provider.num_classes(),
        )
        epoch_history.fid_per_class = self.fid_calculator.get_fid_score_per_class()
        epoch_history.fid = self.fid_calculator.get_fid_scores()

        logger.debug(f"Finished evaluating epoch {epoch_num}")
        logger.debug("starting calculating average current window")
        epoch_history.avg_current_window = self.training_history.get_average(
            start=epoch_num - self.config.avg_window_size + 1, end=epoch_num
        )

        logger.debug("starting calculating average prior window")
        epoch_history.avg_prior_window = self.training_history.get_average(
            start=epoch_num - 2 * self.config.avg_window_size + 1,
            end=epoch_num - self.config.avg_window_size,
        )

        self.generator.train()
        return epoch_history

    def generate_images(self, num_images_per_class):
        """

        :param num_images_per_class:
        :return: tensor of images of shape (num_images_per_class * num_classes, 3, 32, 32)
        """
        logger.debug(f"Generating {num_images_per_class} images per class")
        self.generator.eval()
        generated_images = []
        for class_ in range(self.config.num_classes):
            labels = torch.full(
                (num_images_per_class,), class_, dtype=torch.long, device=self.device
            )
            noise = torch.randn(
                num_images_per_class, self.config.noise_dim, 1, 1, device=self.device
            )
            with torch.no_grad():
                images = self.generator(noise, labels).to(self.device)
            generated_images.append(images)
        logger.debug(f"Finished generating {num_images_per_class} images per class")
        return torch.cat(generated_images, dim=0)

    def _log_trainer_info(self):
        logger.info(
            f"Trainer initialized with the following parameters:\n"
            f"--------Generator--------\n"
            f"{self.generator.model}\n"
            f"--------Discriminator--------\n"
            f"{self.discriminator.model}\n"
            f"--------Configurations--------\n"
            f"{self.config}\n"
            f"criterion: {self.criterion}\n"
            f"Generator optimizer: {self.optimizer_generator}\n"
            f"Discriminator optimizer: {self.optimizer_discriminator}\n"
            f"torch.cuda.is_available(): {torch.cuda.is_available()}\n"
            f"device: {self.device}\n"
            f"keep_best_models: {self.training_history.keep_best_models}\n"
        )

    def _evaluate_inception_score(self, images: torch.Tensor):
        logger.debug(f"Calculating inception score for {images.size(0)} images")
        return inception_score(
            images, device=self.device, batch_size=self.config.batch_size
        )

    def save(self):
        logger.info("Saving the model")
        model_dir = self.config.data_path / "models"
        model_dir.mkdir(parents=True, exist_ok=True)

        for epoch_history in self.training_history.get_history():
            if epoch_history.should_save_model:
                torch.save(
                    obj=epoch_history.model_parameters,
                    f=model_dir / f"generator_{epoch_history.epoch:03}",
                )

            # Save epoch history
            history_file = (
                self.config.log_path / f"epoch_history_{epoch_history.epoch:03}.json"
            )
            with history_file.open("w") as f:
                json.dump(epoch_history.to_dict(), f, indent=4)
