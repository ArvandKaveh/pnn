import collections
from pathlib import Path
from typing import Sequence

import torch
from tqdm import tqdm

from src.logger import get_logger

logger = get_logger("Utils")


def iterate_over(sequence: Sequence, desc="", start=0, end=None):
    end = len(sequence) if end is None else end
    for i in tqdm(range(start, end), desc=desc):
        try:
            data = sequence[i]
        except Exception as e:
            logger.error(f"Error loading from sequence at {desc} {i}: {e}")
            continue
        yield data


def average_checkpoints(inputs: list[Path]) -> str:
    """Loads checkpoints from inputs and returns a model with averaged weights.

    Args:
      inputs: An iterable of string paths of checkpoints to load from.

    Returns:
      A dict of string keys mapping to various values. The 'model' key
      from the returned dict should correspond to an OrderedDict mapping
      string parameter names to torch Tensors.
    """
    if len(inputs) == 1:
        return inputs[0]

    params_dict = collections.OrderedDict()
    params_keys = None
    new_state = None
    num_models = len(inputs)

    for fpath in inputs:
        with open(fpath, "rb") as f:
            state = torch.load(
                f,
                map_location=(
                    lambda s, _: torch.serialization.default_restore_location(s, "cpu")
                ),
            )
        # Copies over the settings from the first checkpoint
        if new_state is None:
            new_state = state

        model_params = state

        model_params_keys = list(model_params.keys())
        if params_keys is None:
            params_keys = model_params_keys
        elif params_keys != model_params_keys:
            raise KeyError(
                "For checkpoint {}, expected list of params: {}, "
                "but found: {}".format(f, params_keys, model_params_keys)
            )

        for k in params_keys:
            p = model_params[k]
            if isinstance(p, torch.HalfTensor):
                p = p.float()
            if k not in params_dict:
                params_dict[k] = p.clone()
                # NOTE: clone() is needed in case of p is a shared parameter
            else:
                params_dict[k] += p

    averaged_params = collections.OrderedDict()
    for k, v in params_dict.items():
        averaged_params[k] = v
        if averaged_params[k].is_floating_point():
            averaged_params[k].div_(num_models)
        else:
            averaged_params[k] //= num_models
    new_state = averaged_params
    save_path = inputs[0].parent / f"checkpoint_avg_top_{num_models}"
    torch.save(new_state, save_path)
    return save_path


if __name__ == "__main__":
    inputs = [
        Path(
            "/Users/arvandkaveh/Projects/kit/pnn/res/base/Generator128D02L4/models/generator_001"
        ),
        Path(
            "/Users/arvandkaveh/Projects/kit/pnn/res/base/Generator128D02L4/models/generator_002"
        ),
    ]

    res = average_checkpoints(inputs)
    print(res)
    print(type(res))
